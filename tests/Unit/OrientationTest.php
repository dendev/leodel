<?php

namespace Tests\Unit;

use Dendev\Leodel\Models\Orientation;
use Orchestra\Testbench\TestCase;

class OrientationTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return ['Dendev\Leodel\AddonServiceProvider'];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $connection = $config['db']['sheldon'];

        $app['config']->set('database.default', 'sheldon');
        $app['config']->set('database.connections.sheldon', $connection);
    }

    public function testGetDomain()
    {
        $orientation_id = 10;
        $orientation = Orientation::find($orientation_id);

        $domain = $orientation->get_domain();
        $this->assertEquals(4, $domain->id_domaine );
    }

    public function testGetClearance()
    {
        $orientation_id = 10;
        $orientation = Orientation::find($orientation_id);

        $clearance = $orientation->get_clearance();
        $this->assertEquals(37, $clearance->id_habilitation);
    }

    public function testGetLessons()
    {
        $orientation_id = 1;
        $orientation = Orientation::find($orientation_id);

        $lessons = $orientation->get_lessons();
        $this->assertGreaterThan(0, count($lessons));
        $this->assertEquals(275053233, $lessons[0]->id_cours);
    }

    public function testGetDepartments()
    {
        $orientation_id = 1;
        $orientation = Orientation::find($orientation_id);

        $departments = $orientation->get_departments();
        $this->assertGreaterThan(0, count($departments));
        $this->assertEquals(9, $departments[0]->id_departement);
    }

    public function testGetYears()
    {
        $orientation_id = 36;
        $orientation = Orientation::find($orientation_id);

        $years = $orientation->get_years();
        $this->assertGreaterThan(0, count($years));
        $this->assertEquals('IV-MSIIA-1M', $years[0]->label_year);
    }

    public function testGetClass()
    {
        $orientation_id = 36;
        $orientation = Orientation::find($orientation_id);

        $classrooms = $orientation->get_classes();
        $this->assertGreaterThan(0, count($classrooms));
        $this->assertEquals('MSIIA-1M-X', $classrooms[0]->label_class);
    }

    public function testGetBloc()
    {
        $orientation_id = 36;
        $orientation = Orientation::find($orientation_id);

        $blocs = $orientation->get_blocs();
        $this->assertGreaterThan(0, count($blocs));
        $this->assertEquals('IV-MSIIA-1M-X', $blocs[0]->label_bloc);
    }
}

