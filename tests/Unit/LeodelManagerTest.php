<?php

namespace Tests\Unit;

use Dendev\Leodel\Models\User;
use Dendev\Leodel\Models\People;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Hash;
use Orchestra\Testbench\TestCase;

class LeodelManagerTest extends TestCase
{
    use DatabaseMigrations;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->_user_people_id = 425277173;
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Leodel\AddonServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $mysql_connection = $config['db']['mysql'];
        $sheldon_connection = $config['db']['sheldon'];

        $app['config']->set('app.env', 'testing');
        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $mysql_connection);
        $app['config']->set('database.connections.sheldon', $sheldon_connection);
        $app['config']->set('auth.providers.users.model', 'Dendev\Leodel\Models\User');
    }

    public function testExist()
    {
        $this->assertEquals(\LeodelManager::test_me(), 'leodel_manager');
    }

    public function testPeopleExist()
    {
        $exist = \LeodelManager::people_exist(14200);
        $not_exist = \LeodelManager::people_exist(99999);

        $this->assertTrue($exist);
        $this->assertFalse($not_exist);
    }

    public function testPeopleFind()
    {
        $exist = \LeodelManager::find_people(2456);
        $not_exist = \LeodelManager::find_people(99999);

        $this->assertInstanceOf(People::class, $exist);
        $this->assertNull($not_exist);
    }

    public function testPeopleFindByNumproeco()
    {
        $exist = \LeodelManager::find_people_by_numproeco(2456);
        $not_exist = \LeodelManager::find_people_by_numproeco(99999);

        $this->assertInstanceOf(People::class, $exist);
        $this->assertNull($not_exist);
    }

    public function testPeopleFindByNameAndFirstname()
    {
        $exist = \LeodelManager::find_people_by_name_and_firstname('De Vos', 'Denis');
        $exist_like = \LeodelManager::find_people_by_name_and_firstname('De%os', 'Den%s');
        $not_exist = \LeodelManager::find_people_by_name_and_firstname('Truc', 'Chose');

        $this->assertInstanceOf(People::class, $exist);
        $this->assertInstanceOf(People::class, $exist_like);
        $this->assertNull($not_exist);
    }

    public function testPeopleFindByEmail()
    {
        $exist = \LeodelManager::find_people_by_email('mdpdevde@henallux.be');
        $not_exist = \LeodelManager::find_people_by_email('truc@henallux.be');

        $this->assertInstanceOf(People::class, $exist);
        $this->assertNull($not_exist);
    }

    // Implantations
    public function testGetImplantationsOfUser()
    {
        $user = $this->_create_user();

        $implantations = \LeodelManager::get_implantations_of_user($user->id);
        $this->assertGreaterThan(0, $implantations->count());
        $this->assertEquals(1, $implantations[0]->numero);
    }

    // Orientations
    public function testGetOrientationsOfUser()
    {
        $user = $this->_create_user();

        $orientations = \LeodelManager::get_orientations_of_user($user->id);
        $this->assertGreaterThan(0, $orientations->count());
        $this->assertEquals(30, $orientations[0]->id_orientation);
    }

    public function testGetOrientationsOfImplantations()
    {
        $implantation_ids = [3];

        $orientations = \LeodelManager::get_orientations_of_implantations($implantation_ids);

        $this->assertGreaterThan(0, $orientations->count());
        $this->assertEquals(23, $orientations[0]->id_orientation);
    }

    public function testGetOrientationsOfImplantation()
    {
        $implantation_id = 3;

        $orientations = \LeodelManager::get_orientations_of_implantation($implantation_id);

        $this->assertGreaterThan(0, $orientations->count());
        $this->assertEquals(23, $orientations[0]->id_orientation);
    }

    // Department
    public function testGetDepartmentsOfUser()
    {
        $user = $this->_create_user();

        $departments = \LeodelManager::get_departments_of_user($user->id);
        $this->assertGreaterThan(0, $departments->count());
        $this->assertEquals(11, $departments[0]->id_departement);
    }

    // Domains
    public function testGetDomainsOfUser()
    {
        $user = $this->_create_user();

        $domains = \LeodelManager::get_domains_of_user($user->id);
        $this->assertGreaterThan(0, $domains->count());
        $this->assertEquals(7, $domains[0]->id_domaine);
    }

    // Sectors
    public function testGetSectorsOfUser()
    {
        $user = $this->_create_user();

        $sectors = \LeodelManager::get_sectors_of_user($user->id);
        $this->assertGreaterThan(0, $sectors->count());
        $this->assertEquals(3, $sectors[0]->id_secteur);
    }

    // Lessons
    public function testGetLessonsOfOrientations()
    {
        $orientation_ids = [23];

        $lessons = \LeodelManager::get_lessons_of_orientations($orientation_ids);

        $this->assertGreaterThan(0, $lessons->count());
        $this->assertEquals(23, $lessons[0]->id_orientation);
    }

    public function testGetLessonsOfOrientation()
    {
        $orientation_id = 23;

        $lessons = \LeodelManager::get_lessons_of_orientation($orientation_id);

        $this->assertGreaterThan(0, $lessons->count());
        $this->assertEquals(23, $lessons[0]->id_orientation);
    }

    public function testGetYearsOfOrientations()
    {
        $orientation_ids = [36];

        $lessons = \LeodelManager::get_years_of_orientations($orientation_ids);

        $this->assertGreaterThan(0, $lessons->count());
        $this->assertEquals('IV-MSIIA-1M', $lessons[0]->label_year);
    }

    public function testGetYearOfOrientation()
    {
        $orientation_id = 36;

        $lessons = \LeodelManager::get_years_of_orientation($orientation_id);

        $this->assertGreaterThan(0, $lessons->count());
        $this->assertEquals('IV-MSIIA-1M', $lessons[0]->label_year);
    }

    public function testGetClassesOfOrientations()
    {
        $orientation_ids = [36];

        $lessons = \LeodelManager::get_classes_of_orientations($orientation_ids);

        $this->assertGreaterThan(0, $lessons->count());
        $this->assertEquals('MSIIA-1M-X', $lessons[0]->label_class);
    }

    public function testGetClassesOfOrientation()
    {
        $orientation_id = 36;

        $lessons = \LeodelManager::get_classes_of_orientation($orientation_id);

        $this->assertGreaterThan(0, $lessons->count());
        $this->assertEquals('MSIIA-1M-X', $lessons[0]->label_class);
    }

    public function testGetBlocsOfOrientations()
    {
        $orientation_ids = [36];

        $lessons = \LeodelManager::get_blocs_of_orientations($orientation_ids);

        $this->assertGreaterThan(0, $lessons->count());
        $this->assertEquals('IV-MSIIA-1M-X', $lessons[0]->label_bloc);
    }

    public function testGetBlocsOfOrientation()
    {
        $orientation_id = 36;

        $lessons = \LeodelManager::get_blocs_of_orientation($orientation_id);

        $this->assertGreaterThan(0, $lessons->count());
        $this->assertEquals('IV-MSIIA-1M-X', $lessons[0]->label_bloc);
    }

    public function testGetUesOfOrientationAndYearsAndBlocs()
    {
        $orientation_id = 36;

        $lessons = \LeodelManager::get_ues_of_orientation_and_years_and_blocs($orientation_id, ['1M'], ['MSIIA']);

        $this->assertGreaterThan(0, $lessons->count());
        $this->assertTrue( $lessons[0]->is_ue());
        $this->assertEquals(243806672, $lessons[0]->id_cours);
    }

    public function testGetAasOfOrientationAndUes()
    {
        $orientation_id = 27;

        $lessons = \LeodelManager::get_aas_of_orientation_and_ues($orientation_id, [256738274]);

        $this->assertGreaterThan(0, $lessons->count());
        $this->assertTrue( $lessons[0]->is_aa());
        $this->assertEquals(243808043, $lessons[0]->id_cours);
    }

    private function _create_user()
    {
        $user = new User();
        $user->name = 'test_1';
        $user->email = 'test_1@gmail.com';
        $user->password = Hash::make('test_1');
        $user->people_id = $this->_user_people_id;

        $user->save();

        return $user;
    }
}
