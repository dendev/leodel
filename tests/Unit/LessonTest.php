<?php

namespace Tests\Unit;

use Dendev\Leodel\Models\Lesson;
use Orchestra\Testbench\TestCase;

class LessonTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return ['Dendev\Leodel\AddonServiceProvider'];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $connection = $config['db']['sheldon'];

        $app['config']->set('database.default', 'sheldon');
        $app['config']->set('database.connections.sheldon', $connection);
    }

    public function testIsActive()
    {
        $active_id = 11769920;
        $inactive_id = 99999999;

        $active_lesson = Lesson::find($active_id);
        $inactive_lesson = Lesson::find($inactive_id);

        $this->assertNotNull($active_lesson);
        $this->assertNull($inactive_lesson);
    }

    public function testIsUe()
    {
        $ue_id = 11769920;
        $aa_id = 11781790;

        $ue_lesson = Lesson::find($ue_id);
        $aa_lesson = Lesson::find($aa_id);

        $this->assertTrue($ue_lesson->is_ue());
        $this->assertFalse($aa_lesson->is_ue());
    }

    public function testIsAa()
    {
        $ue_id = 11769920;
        $aa_id = 11781790;

        $ue_lesson = Lesson::find($ue_id);
        $aa_lesson = Lesson::find($aa_id);

        $this->assertFalse($ue_lesson->is_aa());
        $this->assertTrue($aa_lesson->is_aa());
    }

    public function testHasAa()
    {
        $ue_id = 11769920;
        $aa_id = 11781790;

        $ue_lesson = Lesson::find($ue_id);
        $aa_lesson = Lesson::find($aa_id);

        $this->assertTrue($ue_lesson->has_aa());
        $this->assertFalse($aa_lesson->has_aa());
    }

    public function testGetAas()
    {
        $ue_id = 256738274;
        $aa_id = 243808043;

        $ue_lesson = Lesson::find($ue_id);
        $aa_lesson = Lesson::find($aa_id);

        $ue_aas = $ue_lesson->get_aas();
        $aa_aas = $aa_lesson->get_aas();

        $this->assertGreaterThan(0, $ue_aas->count());
        $this->assertEquals(0, $aa_aas->count());
    }

    public function testGetUe()
    {
        $ue_id = 11769920;
        $aa_id = 11781790;

        $ue_lesson = Lesson::find($ue_id);
        $aa_lesson = Lesson::find($aa_id);

        $ue_ue = $ue_lesson->get_ue();
        $aa_ue = $aa_lesson->get_ue();

        $this->assertFalse($ue_ue);
        $this->assertGreaterThan(0, $aa_ue->count());
    }

    public function testGetStudents()
    {
        // FIXME cours de l'ue existe ou c'est cours des aa de l'ue ?
        //$ue_id = 11802018;
        $aa_id = 243808043;

        //$ue_lesson = Lesson::find($ue_id);
        $aa_lesson = Lesson::find($aa_id);

        //$this->assertGreaterThan(0, $ue_lesson->get_students()->count());
        $this->assertGreaterThan(0, $aa_lesson->get_students()->count());
    }
}

