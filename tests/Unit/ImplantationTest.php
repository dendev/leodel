<?php

namespace Tests\Unit;

use Dendev\Leodel\Models\Implantation;
use Dendev\Leodel\Models\People;
use Orchestra\Testbench\TestCase;

class ImplantationTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return ['Dendev\Leodel\AddonServiceProvider'];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $connection = $config['db']['sheldon'];

        $app['config']->set('database.default', 'sheldon');
        $app['config']->set('database.connections.sheldon', $connection);
    }

    public function testGetDepartments()
    {
        $implantation_id = 8; // bastogne
        $implantation = Implantation::find($implantation_id);

        $departments = $implantation->departments;
        $this->assertGreaterThan(0, count( $departments ) );
        $this->assertEquals('PEBA', $departments[0]->code_proeco );
    }

    public function testGetOrientations()
    {
        $implantation_id = 8; // Bastogne
        $implantation = Implantation::find($implantation_id);

        $orientations = $implantation->get_orientations();
        $this->assertGreaterThan(0, count( $orientations) );
        $this->assertEquals('NSFF', $orientations[0]->code_pe );
    }
}

