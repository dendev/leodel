<?php

namespace Tests\Unit\People;

use Dendev\Leodel\app\Models\People;
use Orchestra\Testbench\TestCase;

class AttributionTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Leodel\AddonServiceProvider',
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            "PeopleManager" => "Dendev\\Leodel\\app\\Facades\\LeodelManagerFacade"
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $connection = $config['db']['sheldon'];

        $app['config']->set('database.default', 'sheldon');
        $app['config']->set('database.connections.sheldon', $connection);
    }

    public function testTmp()
    {
        $this->assertTrue(true); // TODO add real test
    }
    // TODO by persona
    /*
    public function testAttributions()
    {
        $teacher_id = 14201;
        $administrative_id = 14200 ;
        $student_id = 1082;
        $external_id = 14064;

        $teacher = People::find($teacher_id);
        $administrative = People::find($administrative_id);
        $student = People::find($student_id);
        $external = People::find($external_id);

        $teacher_attributions = $teacher->get_attributions($teacher_id);
        $administrative_attributions = $administrative->get_attributions($administrative_id);
        $student_attributions = $student->get_attributions($student_id);
        $external_attributions = $external->get_attributions($external_id);

        $this->assertGreaterThan(0, count( $teacher_attributions) );
        $this->assertGreaterThan(0, count( $administrative_attributions) );
        $this->assertEquals(0, count( $student_attributions) ); // student dont have attribution
        $this->assertGreaterThan(0, count( $external_attributions) );
    }
    */
}

