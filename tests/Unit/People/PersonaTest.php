<?php

namespace Tests\Unit\People;

use Dendev\Leodel\Models\People;
use Orchestra\Testbench\TestCase;

class PersonaTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->not_teacher_id = 14200; // mdp
        $this->teacher_id = 14201; // teacher
        $this->inactive_teacher_id = 13563;

        $this->not_student_id = 14200; // mdp
        $this->student_id = 10079; // student

        $this->not_administrative_id = 14201; // teacher
        $this->administrative_id = 14200; // mdp

        $this->external_id = 378457175;
        $this->not_external_id = 14201;
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Leodel\AddonServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $connection = $config['db']['sheldon'];

        $app['config']->set('database.default', 'sheldon');
        $app['config']->set('database.connections.sheldon', $connection);
    }

    public function testIsTeacher()
    {
        $not_teacher = People::find( $this->not_teacher_id);
        $teacher = People::find( $this->teacher_id);

        $is_not_teacher = $not_teacher->is_teacher();
        $is_teacher = $teacher->is_teacher();

        $this->assertFalse($is_not_teacher);
        $this->assertTrue($is_teacher);
    }

    public function testIsStudent()
    {
        $not_student = People::find($this->not_student_id);
        $student = People::find($this->student_id);

        $is_not_student = $not_student->is_student($this->not_student_id);
        $is_student = $student->is_student($this->student_id);

        $this->assertFalse($is_not_student);
        $this->assertTrue($is_student);
    }

    public function testIsAdministrative()
    {
        $not_administrative = People::find($this->not_administrative_id);
        $administrative = People::find($this->administrative_id);

        $is_not_administrative = $not_administrative->is_administrative($this->not_administrative_id);
        $is_administrative = $administrative->is_administrative($this->administrative_id);

        $this->assertFalse($is_not_administrative);
        $this->assertTrue($is_administrative);
    }

    public function testIsExternal()
    {
        $external = People::find($this->external_id);
        $not_external = People::find($this->not_external_id);

        $is_external = $external->is_external($this->external_id);
        $is_not_external = $not_external->is_external($this->not_external_id);

        $this->assertFalse($is_not_external);
        $this->assertTrue($is_external);
    }

    public function testGetActiveStudent()
    {
        $student = People::find($this->student_id);
        $not_student = People::find($this->not_student_id);

        $students = $student->get_personas_student();
        $not_students = $not_student->get_personas_student();

        $this->assertTrue($students->count() > 0);
        $this->assertTrue($not_students->count() == 0);
    }

    public function testGetActiveTeacher()
    {
        $teacher = People::find($this->teacher_id);
        $inactive_teacher = People::find($this->inactive_teacher_id);
        $not_teacher = People::find($this->not_teacher_id);

        $teachers = $teacher->get_personas_teacher();
        $inactive_teachers = $inactive_teacher->get_personas_teacher();
        $not_teachers = $not_teacher->get_personas_teacher();

        $this->assertGreaterThan(0, $teachers->count());
        $this->assertEquals(0, $inactive_teachers->count());
        $this->assertEquals(0, $not_teachers->count());
    }

    public function testGetActiveAdministrative()
    {
        $administrative = People::find($this->administrative_id);
        $not_administrative = People::find($this->not_administrative_id);

        $administratives = $administrative->get_personas_administrative();
        $not_administratives = $not_administrative->get_personas_administrative();

        $this->assertGreaterThan(0, $administratives->count());
        $this->assertEquals(0, $not_administratives->count());
    }

    public function testGetActiveExternal()
    {
        $external = People::find($this->external_id);
        $not_external = People::find($this->not_external_id);

        $externals = $external->get_personas_external();
        $not_externals = $not_external->get_personas_external();

        $this->assertGreaterThan(0, $externals->count());
        $this->assertEquals(0, $not_externals->count());
    }

    // TEST get personas get specific personas
}

