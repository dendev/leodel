<?php

namespace Tests\Unit\People;

use Dendev\Leodel\Models\People;
use Orchestra\Testbench\TestCase;

class DepartmentTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Leodel\AddonServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $connection = $config['db']['sheldon'];

        $app['config']->set('database.default', 'sheldon');
        $app['config']->set('database.connections.sheldon', $connection);
    }

    public function testDepartments()
    {
        $teacher_id = 14201;
        $administrative_id = 14200 ;
        $student_id = 1082;
        $external_id = 378457175;

        $teacher = People::find($teacher_id);
        $administrative = People::find($administrative_id);
        $student = People::find($student_id);
        $external = People::find($external_id);

        $teacher_departments = $teacher->get_departments($teacher_id);
        $administrative_departments = $administrative->get_departments($administrative_id);
        $student_departments = $student->get_departments($student_id);
        $external_departments = $external->get_departments($external_id);

        $this->assertGreaterThan(0, count( $teacher_departments) );
        $this->assertEquals($teacher_departments['teacher'][0]->code_proeco, 'ECON' );

        $this->assertGreaterThan(0, count( $administrative_departments) );
        $this->assertEquals($administrative_departments['administrative'][0]->code_proeco, 'Z_JURIDIQUE' );

        $this->assertGreaterThan(0, count( $student_departments) );
        $this->assertEquals($student_departments['student'][0]->code_proeco, 'MAPEMASS' );

        $this->assertGreaterThan(0, count( $external_departments) );
        $this->assertEquals($external_departments['external'][0]->code_proeco, 'PECH' );
    }

    public function testGetInactiveDepartmentByPersona()
    {
        $student_id = 22446083;
        $teacher_id = 13563;
        $administrative_id = 13571;
        $external_id = 378457200;

        $student = People::find($student_id);
        $teacher = People::find($teacher_id);
        $administrative = People::find($administrative_id);
        $external = People::find($external_id);

        $student_departments = $student->get_departments_of_persona('student', 'inactive');
        $teacher_departments = $teacher->get_departments_of_persona('teacher', 'inactive');
        $administrative_departments = $administrative->get_departments_of_persona('administrative', 'inactive');
        $external_departments = $external->get_departments_of_persona('external', 'inactive');

        $this->assertGreaterThan(0, count( $student_departments ) );
        $this->assertGreaterThan(0, count( $teacher_departments ) );
        $this->assertGreaterThan(0, count( $administrative_departments ) );
        $this->assertGreaterThan(0, count( $external_departments ) );
    }

    public function testGetStudentDepartments()
    {
        $id = 1271;
        $people = People::find( $id );
        $departments = $people->get_departments_student();
        $this->assertGreaterThan(0, count( $departments ) );
    }

    public function testGetTeacherDepartments()
    {
        $id = 14201;
        $people = People::find( $id );
        $departments = $people->get_departments_teacher();
        $this->assertGreaterThan(0, count( $departments ) );
    }

    public function testGetAdministrativeDepartments()
    {
        $id = 14200;
        $people = People::find( $id );
        $departments = $people->get_departments_administrative();
        $this->assertGreaterThan(0, count( $departments ) );
    }

    public function testGetExternalDepartments()
    {
        $id = 378457175;
        $people = People::find( $id );
        $departments = $people->get_departments_external();
        $this->assertGreaterThan(0, count( $departments ) );
    }

    public function testInactiveDepartments()
    {
        $one_administrative_id = 13777;
        $one_teacher_id = 13563;
        $one_student_id = 350;
        $one_external_id = 378457200;

        $one_administrative = People::find($one_administrative_id);
        $one_teacher = People::find($one_teacher_id);
        $one_student = People::find($one_student_id);
        $one_external = People::find($one_external_id);

        $one_administrative = $one_administrative->get_departments('inactive');
        $one_teacher = $one_teacher->get_departments('inactive');
        $one_student = $one_student->get_departments('inactive');
        $one_external = $one_external->get_departments('inactive');

        $this->assertGreaterThan(0, count( $one_administrative['administrative'] ) );
        $this->assertEquals($one_administrative['administrative'][0]->code_proeco, 'PECH' );

        $this->assertGreaterThan(0, count( $one_teacher['teacher'] ) );
        $this->assertEquals($one_teacher['teacher'][0]->code_proeco, 'PECH' );

        $this->assertGreaterThan(0, count( $one_student['student'] ) );
        $this->assertEquals($one_student['student'][0]->code_proeco, 'SOMA' );

        $this->assertGreaterThan(0, count( $one_external['external'] ) );
        $this->assertEquals($one_external['external'][0]->code_proeco, 'PARA' );
    }

    public function testUndistinctInactiveDepartments() // TODO check multi departments
    {
        $one_administrative_id = 13777;
        $one_teacher_id = 13563;
        $one_student_id = 350;
        $one_external_id = 13748;

        $one_administrative = People::find($one_administrative_id);
        $one_teacher = People::find($one_teacher_id);
        $one_student = People::find($one_student_id);
        $one_external = People::find($one_external_id);

        $one_administrative = $one_administrative->get_departments('inactive', false);
        $one_teacher = $one_teacher->get_departments('inactive', false);
        $one_student = $one_student->get_departments('inactive', false);
        $one_external = $one_external->get_departments('inactive', false);

        $this->assertGreaterThan(0, count( $one_administrative ) );
        $this->assertGreaterThan(0, count( $one_teacher ) );
        $this->assertGreaterThan(0, count( $one_student ) );
        $this->assertGreaterThan(0, count( $one_external ) );
    }
}

