<?php

namespace Tests\Unit\People;

use Dendev\Leodel\Models\People;
use Orchestra\Testbench\TestCase;

class OrientationTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Leodel\AddonServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $connection = $config['db']['sheldon'];

        $app['config']->set('database.default', 'sheldon');
        $app['config']->set('database.connections.sheldon', $connection);
    }

    public function testGetOrientationByPersona()
    {
        $student_id = 262603943;
        $teacher_id = 14201;
        $administrative_id = 14200;
        $external_id = 155517479;

        $student = People::find($student_id);
        $teacher = People::find($teacher_id);
        $administrative = People::find($administrative_id);
        $external = People::find($external_id);

        $student_orientations = $student->get_orientations_of_persona('student');
        $teacher_orientations = $teacher->get_orientations_of_persona('teacher');
        $administrative_orientations = $administrative->get_orientations_of_persona('administrative');
        $external_orientations = $external->get_orientations_of_persona( 'external');

        $this->assertGreaterThan(0, count( $student_orientations ) );
        $this->assertGreaterThan(0, count( $teacher_orientations ) );
        $this->assertEquals(0, count( $administrative_orientations ) );
        $this->assertEquals(0, count( $external_orientations ) );
    }

    public function testGetStudentOrientations()
    {
        $id = 262603943;
        $people = People::find( $id );
        $orientations = $people->get_orientations_student();
        $this->assertGreaterThan(0, count( $orientations ) );
    }

    public function testGetTeacherOrientations()
    {
        $id = 14201;
        $people = People::find( $id );
        $orientations = $people->get_orientations_teacher();
        $this->assertGreaterThan(0, count( $orientations ) );
    }

    public function testGetAdministrativeOrientations()
    {
        $id = 14200;
        $people = People::find( $id );
        $orientations = $people->get_orientations_administrative();
        $this->assertEquals(0, count( $orientations ) );
    }

    public function testGetExternalOrientations()
    {
        $id = 155517479;
        $people = People::find( $id );
        $orientations = $people->get_orientations_external();
        $this->assertEquals(0, count( $orientations ) );
    }
}
