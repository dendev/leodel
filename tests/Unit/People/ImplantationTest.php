<?php

namespace Tests\Unit\People;

use Dendev\Leodel\Models\People;
use Orchestra\Testbench\TestCase;

class ImplantationTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->one_administrative_id = 14200;
        $this->one_teacher_id = 14201;
        $this->one_student_id = 1082;
        $this->one_external_id = 378457175;
        $this->one_external_inactive_id = 378457200;
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Leodel\AddonServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $connection = $config['db']['sheldon'];

        $app['config']->set('database.default', 'sheldon');
        $app['config']->set('database.connections.sheldon', $connection);
    }

    public function testIsTeacherImplantation()
    {
        $student_id = 1271;
        $teacher_id = 14201;

        $student = People::find($student_id);
        $teacher = People::find($teacher_id);

        $this->assertEmpty($student->is_teacher_implantation());
        $this->assertTrue($teacher->is_teacher_implantation());
        $this->assertTrue($teacher->is_teacher_implantation(2)); //id_implantation 2 == IESN
        $this->assertTrue($teacher->is_teacher_implantation(1, 'active', 'number')); //number 1 == IESN
    }

    public function testIsStudentImplantation()
    {
        $student_id = 1271;

        $student = People::find($student_id);

        $this->assertTrue($student->is_student_implantation());
        $this->assertTrue($student->is_student_implantation(2)); //id_implantation 2 == IESN
        $this->assertTrue($student->is_student_implantation(1, 'active', 'number')); //number 1 == IESN
    }

    public function testIsAdministrativeImplantation()
    {
        $administrative_id = 14200;

        $administrative = People::find($administrative_id);

        $this->assertTrue($administrative->is_administrative_implantation());
        $this->assertTrue($administrative->is_administrative_implantation(13)); //id_implantation 13 == SC
        $this->assertTrue($administrative->is_administrative_implantation(12, 'active', 'number')); //number 12 == SC
    }

    public function testIsExternalImplantation()
    {
        $external_id = 378457175;

        $external = People::find($external_id);

        $this->assertTrue($external->is_external_implantation());
        $this->assertTrue($external->is_external_implantation(5)); //id_implantation 3 == PA
        $this->assertTrue($external->is_external_implantation(4, 'active', 'number')); //number 2 == PA
    }

    public function testIsPersonaImplantation()
    {
        $student_id = 1271;
        $teacher_id = 14201;
        $administrative_id = 14200;

        $student = People::find($student_id);
        $teacher = People::find($teacher_id);
        $administrative = People::find($administrative_id);
        $external = People::find($this->one_external_id);

        $this->assertTrue($administrative->is_persona_implantation('administrative', 13, 'active', 'id'));

        $this->assertTrue($student->is_persona_implantation('student', 2, 'active', 'id'));
        $this->assertTrue($student->is_persona_implantation('student', 1, 'active', 'numero'));

        $this->assertTrue($teacher->is_persona_implantation('teacher', 2, 'active', 'id'));
        $this->assertTrue($teacher->is_persona_implantation('teacher', 1, 'active', 'numero'));

        $this->assertTrue($administrative->is_persona_implantation('administrative', 13, 'active', 'id'));
        $this->assertTrue($administrative->is_persona_implantation('administrative', 12, 'active', 'numero'));

        $this->assertTrue($external->is_persona_implantation('external', 5, 'active', 'id'));
        $this->assertTrue($external->is_persona_implantation('external', 4, 'active', 'numero'));
    }

    public function testGetImplantationByPersona()
    {
        $student_id = 1271;
        $teacher_id = 14201;
        $administrative_id = 14200;

        $student = People::find($student_id);
        $teacher = People::find($teacher_id);
        $administrative = People::find($administrative_id);
        $external = People::find($this->one_external_id);

        $student_implantations = $student->get_implantations_of_persona('student');
        $teacher_implantations = $teacher->get_implantations_of_persona('teacher');
        $administrative_implantations = $administrative->get_implantations_of_persona('administrative');
        $external_implantations = $external->get_implantations_of_persona( 'external');

        $this->assertGreaterThan(0, count( $student_implantations ) );
        $this->assertGreaterThan(0, count( $teacher_implantations ) );
        $this->assertGreaterThan(0, count( $administrative_implantations ) );
        $this->assertGreaterThan(0, count( $external_implantations ) );
    }

    public function testGetInactiveImplantationByPersona()
    {
        $student_id = 22446083;
        $teacher_id = 13563;
        $administrative_id = 13571;

        $student = People::find($student_id);
        $teacher = People::find($teacher_id);
        $administrative = People::find($administrative_id);
        $external = People::find($this->one_external_inactive_id);

        $student_implantations = $student->get_implantations_of_persona('student', 'inactive');
        $teacher_implantations = $teacher->get_implantations_of_persona('teacher', 'inactive');
        $administrative_implantations = $administrative->get_implantations_of_persona('administrative', 'inactive');
        $external_implantations = $external->get_implantations_of_persona('external', 'inactive');

        $this->assertGreaterThan(0, count( $student_implantations ) );
        $this->assertGreaterThan(0, count( $teacher_implantations ) );
        $this->assertGreaterThan(0, count( $administrative_implantations ) );
        $this->assertGreaterThan(0, count( $external_implantations ) );
    }

    public function testGetStudentImplantations()
    {
        $id = 1271;
        $people = People::find( $id );
        $implantations = $people->get_implantations_student();
        $this->assertGreaterThan(0, count( $implantations ) );
    }

    public function testGetTeacherImplantations()
    {
        $id = 14201;
        $people = People::find( $id );
        $implantations = $people->get_implantations_teacher();
        $this->assertGreaterThan(0, count( $implantations ) );
    }

    public function testGetAdministrativeImplantations()
    {
        $id = 14200;
        $people = People::find( $id );
        $implantations = $people->get_implantations_administrative();
        $this->assertGreaterThan(0, count( $implantations ) );
    }

    public function testGetExternalImplantations()
    {
        $people = People::find( $this->one_external_id);
        $implantations = $people->get_implantations_external();
        $this->assertGreaterThan(0, count( $implantations ) );
    }

    public function testImplantations() // TODO check multi implantations
    {
        $one_administrative = People::find($this->one_administrative_id);
        $one_teacher = People::find($this->one_teacher_id);
        $one_student = People::find($this->one_student_id);
        $one_external = People::find($this->one_external_id);

        $one_administrative = $one_administrative->get_implantations();
        $one_teacher = $one_teacher->get_implantations();
        $one_student = $one_student->get_implantations();
        $one_external = $one_external->get_implantations();

        $this->assertGreaterThan(0, count( $one_administrative['administrative'] ) );
        $this->assertEquals($one_administrative['administrative'][0]->numero, 12 );

        $this->assertGreaterThan(0, count( $one_teacher['teacher'] ) );
        $this->assertEquals($one_teacher['teacher'][0]->numero, 1 );

        $this->assertGreaterThan(0, count( $one_student['student'] ) );
        $this->assertEquals($one_student['student'][0]->numero, 2 );

        $this->assertGreaterThan(0, count( $one_external['external'] ) );
        $this->assertEquals($one_external['external'][0]->numero, 4 );
    }

    public function testInactiveImplantations() // TODO check multi implantations
    {
        $one_administrative = People::find($this->one_administrative_id);
        $one_teacher = People::find($this->one_teacher_id);
        $one_student = People::find($this->one_student_id);
        $one_external = People::find($this->one_external_inactive_id);

        $one_administrative = $one_administrative->get_implantations('inactive');
        $one_teacher = $one_teacher->get_implantations('inactive');
        $one_student = $one_student->get_implantations('inactive');
        $one_external = $one_external->get_implantations('inactive');

        $this->assertGreaterThan(0, count( $one_administrative['administrative'] ) );
        $this->assertEquals($one_administrative['administrative'][0]->numero, 12 );

        $this->assertGreaterThan(0, count( $one_teacher['teacher'] ) );
        $this->assertEquals($one_teacher['teacher'][0]->numero, 1 );

        $this->assertGreaterThan(0, count( $one_student['student'] ) );
        $this->assertEquals($one_student['student'][0]->numero, 2 );

        $this->assertGreaterThan(0, count( $one_external['external'] ) );
        $this->assertEquals($one_external['external'][0]->numero, 2 );
    }

    public function testUndistinctInactiveImplantations() // TODO check multi implantations
    {
        $one_administrative = People::find($this->one_administrative_id);
        $one_teacher = People::find($this->one_teacher_id);
        $one_student = People::find($this->one_student_id);
        $one_external = People::find($this->one_external_inactive_id);

        $one_administrative = $one_administrative->get_implantations('inactive', false);
        $one_teacher = $one_teacher->get_implantations('inactive', false);
        $one_student = $one_student->get_implantations('inactive', false);
        $one_external = $one_external->get_implantations('inactive', false);

        $this->assertGreaterThan(0, count( $one_administrative ) );
        $this->assertGreaterThan(0, count( $one_teacher ) );
        $this->assertGreaterThan(0, count( $one_student) );
        $this->assertGreaterThan(0, count( $one_external ) );
    }
}
