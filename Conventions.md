# Conventions

## Nommage
### Structure
Organisé comme suit:   
**Action->Sujet->Variant**

Idéalement des mots lien of, for, in peuvent venir clarifier le variant

### Actions
* find: recheche
* get: récupère et renvoi
* is: pose une question, retourn un bool

### Args
L'argument par défaut est celui de sheldon ( principalement l'id people).   
Pour rendre la ressources récupérablle plus facilement l'utlisation de méthode soeurs est possible.  
Elles font le mm travail mais partent d'un argument différent.   
Leur nom contient un by qui indique par quelle type d'argument elle vont être utilisée.   

**find_people**: iRetourne le people dont l'id est passé
**find_people_by_matricule**:Alias find_people_by_numproeco
**find_people_by_numproeco**:Fournit le people dont le numero proeco est donnée
**find_people_by_email**:Fournit le people dont l'email henallux est donnée

### Exemples   
**is_student**: indique si est ou pas un étudiant   
**is_student_in_implantation**: indique si est ou pas un étudiant dans l'implantation donnée en argument     
**get_departments_administrative**: retourne les départements où le people à un role administratif   
**find_people**: recheche le people dont l'id est donnée en arg   

## Structure 
Leodel se base sur des models qui exploitent les tables de la db sheldon.      
C'est model sont éttoffer de méthodes utiles en lien avec le model.     
La couche models qui n'existe que pour accéder et founir la donnée d'une table.   
La réconciliation de donnée se fait plus haut.

S'il est besoin d'une metode utilisant plusieurs model on va placer celle ci dans le service ( LeodelManagerService).   
Le service propose une abstraction plus forte et permet d'ignorer la couche models.   

Le service à pour vocation de fournir les données les plus courantes.
Permettre au développeur de ne pas se poser la question de comment obtenir telle ou telle donnée.

Le service utilise du code générique qui est private et dont l'utilisation est rendu accessible via des méthode public qui se contentent de paramétré et appeler la méthode privé.
L'utilisateur du package ne doit s'intérésser qu'au méthode public.     
Le développeut voulant ajouter des méthodes doit regarder si il existe des methodes privates qui lui sont utiles.   
Les méthodes privées sont préfixés avec _

## Alias
Un trait Aliasing est disponible pour ajouter des alias aux methodes de LeodelManagerService.

Exemple:  
Existe dans LeodelManagerService la méthode **find_people_by_matricule**.
On désire l'appeler différement en ajoutant dans aliasing :
```php
public function trouver_people_par_son_matricule($numproeco)
{
    return $this->find_people_by_matricule($numproeco);
}
```
Ce qui permet ensuite de faire 
```php 
\LeodelManager::find_people_by_matricule(1);
\LeodeLManager::trouver_people_par_son_matricule(1);
```

Leodel utilise le snakecase mais la methode magic __call présent dans aliasing "traduit" en camelcase.   
Il est possible de faire
```php 
\LeodelManager::find_people_by_matricule(1);
\LeodelManager::findPeopleByMatricule(1);
\LeodelManager::trouver_people_par_son_matricule(1);
\LeodelManager::trouverPeopleParSonMatricule(1);
```
