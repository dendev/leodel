<?php

return [
    'sheldon' => [
        'connection' => [
            'driver' => 'pgsql',
            'url' => env('DATABASE_SHELDON_URL'),
            'host' => env('DB_SHELDON_HOST', '127.0.0.1'),
            'port' => env('DB_SHELDON_PORT', '5432'),
            'database' => env('DB_SHELDON_DATABASE', 'forge'),
            'username' => env('DB_SHELDON_USERNAME', 'forge'),
            'password' => env('DB_SHELDON_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => env('DB_SHELDON_SCHEMA', 'public'),
            'sslmode' => 'prefer',
        ],
    ],
];
