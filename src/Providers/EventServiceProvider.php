<?php

namespace Dendev\Leodel\Providers;

use Dendev\Leodel\Events\PeopleRetrieved as PeopleRetrievedEvent;
use Dendev\Leodel\Listeners\PeopleRetrieved as PeopleRetrievedListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        PeopleRetrievedEvent::class => [
            PeopleRetrievedListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
