<?php

namespace Dendev\Leodel\Providers;

use Dendev\Leodel\Console\Commands\Install;
use Dendev\Leodel\Console\Commands\PublishPeopleModel;
use Dendev\Leodel\Providers\EventServiceProvider;
use Dendev\Leodel\Services\LeodelManagerService;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class LeodelServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // service
        $this->app->singleton('leodel_manager', function ($app) {
            return new LeodelManagerService();
        });

        // register providers
        $this->app->register(EventServiceProvider::class);
        $this->app->register('Dendev\AcademicYear\AcademicYearServiceProvider');

        // alias
        $loader = AliasLoader::getInstance();
        $loader->alias('LeodelManager', 'Dendev\Leodel\LeodelManagerFacade');
        $loader->alias('AcademicYearManager', 'Dendev\AcademicYear\AcademicYearManagerFacade');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // config
        $this->publishes([
            __DIR__.'/../config/leodel.php' => config_path('template.php'),
        ]);

        // db
        $this->_set_sheldon_connection();

        // cmd
        if ($this->app->runningInConsole()) {
            $this->commands([
                PublishPeopleModel::class,
                Install::class,
            ]);
        }
    }

    // -
    private function _set_sheldon_connection()
    {
        $connection = Config::get('leodel.sheldon.connection');
        if( $connection )
        {
            Config::set("database.connections.sheldon", $connection);
        }
        else
        {
            if( ! \App::environment() )
                echo 'Run php artisan vendor:publish --provider="Dendev\Leodel\LeodelServiceProvider"';

        }
    }
}
