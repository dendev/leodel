<?php
namespace Dendev\Leodel\Traits;

trait Aliasing
{
    /**
     * Alias find_people_by_numproeco
     *
     * @see find_people_by_numproeco
     *
     * @param string $numproeco numéro proeco du people que l'on veut obtenir
     * @return People|null Model People si trouvé sinon Null
     */
    public function trouver_people_par_son_matricule($numproeco)
    {
        return $this->find_people_by_matricule($numproeco);
    }

    public function __call($name, $arguments)
    {
        $snake_case_name = $this->_to_snake_case($name);

        return $this->$snake_case_name(...$arguments);
    }

    private function _to_snake_case($name)
    {
        $output = ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $name)), '_');
        return $output;
    }
}
