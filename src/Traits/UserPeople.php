<?php

namespace Dendev\Leodel\Traits;

/**
 * Permet au model User de laravel de travailler avec Sheldon.
 *
 * Le trait permet de faire le lien entre l'utilisateur laravel possédant un email
 * et son People existant dans sheldon
 *
 * Trait UserPeople
 * @package Dendev\Leodel\Traits
 */
trait UserPeople
{
    /**
     * Retourne l'identity de l'utilisateur.
     *
     * L'identitiy est extrait de l'email ( mdpdevde@henallux.be -> mdpdevde )
     *
     * @return string identité du people
     */
    public function getIdentityAttribute() : string
    {
        return substr($this->email, 0, strpos($this->email, "@"));
    }

    /**
     * Indique si l'utilisateur laravel existe dans la table people de Sheldon
     * @return bool
     */
    public function is_people() : bool
    {
        return ( is_null($this->people_id) ) ? false: true;
    }

    /**
     * Relation faisant le lien avec le model People
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne le model people de l'user
     */
    public function people()
    {
        return $this->hasOne('Dendev\Leodel\Models\People', 'id_people', 'people_id');
    }

    // TODO
    // get name firstname from sheldon && save in db -> update personal infos
}
