<?php


namespace Dendev\Leodel\Traits;


trait UtilModel
{
    /**
     * Permet l'ajout de filtre sur des request
     *
     * @param $rqs
     * @param $extras
     * @return mixed
     * @throws \Exception
     */
    private function _add_extra_filters($rqs, $extras)
    {
        if( ! is_array($extras) )
            throw new \Exception(__METHOD__ . '# Réclame un tableau en second argument');

        foreach ($extras as $extra)
        {
            $column = $extra['column'];
            $operator = $extra['operator'];
            $value = $extra['value'];

            $rqs = $rqs->where($column, $operator, $value);
        }

        return $rqs;
    }
}

