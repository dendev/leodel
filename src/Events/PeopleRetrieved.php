<?php
namespace Dendev\Leodel\Events;

use Dendev\Leodel\Models\People;
use Illuminate\Queue\SerializesModels;

class PeopleRetrieved
{
    use SerializesModels;

    public $people;

    public function __construct(People $people)
    {
        $this->people = $people;
    }

}
