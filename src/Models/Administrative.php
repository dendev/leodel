<?php
namespace Dendev\Leodel\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Travail avec la table administratifs et est en lien avec people et implantations
 *
 * Class Administrative
 * @package Dendev\Leodel\Models
 */
class Administrative extends Model
{
    //use SoftDeletes;
    //use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $connection = 'sheldon';
    protected $table = 'administratifs';
    protected $primaryKey = 'id_administratif';
    // public $timestamps = false;
    protected $guarded = ['id_administratifs'];
    protected $appends = ['id', 'key', 'value', 'label'];
    protected $fillable = [];
    // protected $hidden = [];
    protected $dates = ['date_entree', 'date_fin'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    /**
     * Relation avec implantation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function implantation()
    {
        return $this->hasOne('Dendev\Leodel\Models\Implantation', 'numero', 'numero');
    }

    /**
     * Relation avec people
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function people()
    {
        return $this->belongsTo('Dendev\Leodel\Models\People', 'id_people', 'id_people');
    }

    /**
     * Relation avec user ( user de laravel pas de sheldon )
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User')->using('Dendev\Leodel\Models\People');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    /**
     * Ajoute au model l'attribut id
     * @return string id du model
     */
    public function getIdAttribute()
    {
        return $this->id_administratif;
    }

    /**
     * Ajoute au model un attribut key ( à destination de liste UI )
     * @return string id du model
     */
    public function getKeyAttribute()
    {
        return $this->id;
    }

    /**
     * Ajoute au model un attribut label pour l'affichage
     * @return string label
     */
    public function getLabelAttribute()
    {
        return $this->email_ad;
    }

    /**
     * Ajoute au model un attribut value ( à destination de liste UI )
     * @return string la valeur affichable
     */
    public function getValueAttribute()
    {
        return $this->label;
    }


    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
