<?php

namespace Dendev\Leodel\Models;

use Dendev\Leodel\Traits\UtilModel;
use Illuminate\Database\Eloquent\Model;

class Orientation extends Model
{
    use UtilModel;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $connection = 'sheldon';
    protected $table = 'orientations';
    protected $primaryKey = 'id_orientation';
    // public $timestamps = false;
    protected $guarded = ['id_orientation'];
    protected $appends = ['id', 'key', 'value', 'label'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $identity;

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */


    /**
     * Fournit le domain en lien avec l'orientation
     *
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return mixed
     */
    public function get_domain($extras = [])
    {
        $rqs = $this->domain();

        $rqs = $this->_add_extra_filters($rqs, $extras);

        return $rqs->first();
    }

    /**
     * Fournit l'habilitation en lien avec l'orientation
     *
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return mixed
     */
    public function get_clearance($extras = [])
    {
        $rqs = $this->clearance();

        $rqs = $this->_add_extra_filters($rqs, $extras);

        return $rqs->first();
    }

    /**
     * Fournit tous les departements en lien avec l'orientation
     *
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return mixed
     */
    public function get_departments($extras = [])
    {
        $rqs = $this->departments();

        $rqs = $this->_add_extra_filters($rqs, $extras);

        return $rqs->get();
    }

    /**
     * Fournit tous les cours en lien avec l'orientation
     *
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return mixed
     */
    public function get_lessons($filter = 'active', $extras = []) // FIXME dinstinct && undinstinct -> group by ?
    {
        $rqs = $this->lessons();

        // active
        $rqs = $this->_add_active_filter($rqs, $filter);

        // extras
        $rqs = $this->_add_extra_filters($rqs, $extras);

        return $rqs->get();
    }

    /**
     * Fournit tous les blocs en lien avec l'orientation
     *
     * Bloc est une notion extraite de la table cours et renvoi des models Lessons
     *
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return mixed
     */
    public function get_blocs($filter = 'active', $extras = []) // TODO add test
    {
        $rqs = $this->lessons()
            ->where('id_orientation', $this->id_orientation)
            ->where('bloc_14', '<>', '');

        // active
        $rqs = $this->_add_active_filter($rqs, $filter);

        // extras
        $rqs = $this->_add_extra_filters($rqs, $extras);

        return $rqs->get()->unique('bloc_14');
    }

    /**
     * Fournit toutes les années en lien avec l'orientation
     *
     * Années est une notion extraite de la table cours et renvoi des models Lessons.
     * Une Année est le "bloc" d'études ex: 1B, 2B ...
     *
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return mixed
     */
    public function get_years($filter = 'active', $extras = []) // 1B, 2B ...
    {
        $rqs = $this->lessons()
            ->where('id_orientation', $this->id)
            //->whereNull('classe')
            ->orderBy('code_pe');

        // active
        $rqs = $this->_add_active_filter($rqs, $filter);

        // extras
        $rqs = $this->_add_extra_filters($rqs, $extras);

        // datas
        $datas = $rqs->get()->unique('annee');

        // identity
        $years = $this->_set_lesson_identity($datas, 'year');

        return $years;
    }

    /**
     * Fournit tous les classes en lien avec l'orientation
     *
     * Classe est une notion extraite de la table cours et renvoi des models Lessons
     * La classe est une unité d'organisation pour dispenser les cours ex A,B,C, ...
     *
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return mixed
     */
    public function get_classes($filter = 'active', $extras = []) // TODO waiting classe column
    {
        $rqs = $this->lessons()
            ->where('id_orientation', $this->id)
            //->whereNull('classe')
            //->where('annee_academique', $academic_year)
            ->orderBy('code_pe');

        // active
        $rqs = $this->_add_active_filter($rqs, $filter);

        // extras
        $rqs = $this->_add_extra_filters($rqs, $extras);

        // datas
        $datas = $rqs->get()->unique('classe');

        // identity
        $classes = $this->_set_lesson_identity($datas, 'class');

        return $classes;
    }

    public function get_ues($filter = 'active', $extras = [])
    {
        $rqs = $this->lessons()
            ->where('id_orientation', $this->id)
            ->where('cours_ue', '<>', '')
            ->orderBy('code');

        // active
        $rqs = $this->_add_active_filter($rqs, $filter);

        // extras
        $rqs = $this->_add_extra_filters($rqs, $extras);

        // datas
        $datas = $rqs->get()->unique('code');

        // identity
        $ues = $this->_set_lesson_identity($datas, 'ue');

        return $ues;
    }


    public function get_aas($filter = 'active', $extras = [])
    {
        $rqs = $this->lessons()
            ->where('id_orientation', $this->id)
            ->where('cours_ue', '=', '')
            ->orderBy('code');

        // active
        $rqs = $this->_add_active_filter($rqs, $filter);

        // extras
        $rqs = $this->_add_extra_filters($rqs, $extras);

        // datas
        $datas = $rqs->get()->unique('code');

        // identity
        $aas = $this->_set_lesson_identity($datas, 'aa');

        return $aas;
    }

    public function get_aas_of_ues($year_values, $bloc_values, $filter = 'active')
    {
        $wheres = [];
        foreach( $year_values as $year_value )
            $wheres[] = ['column' => 'annee', 'operator' => '=', 'value' => $year_value];
        foreach( $bloc_values as $bloc_value )
            $wheres[] = ['column' => 'code_pe', 'operator' => '=', 'value' => $bloc_value];

        return $this->get_ues($filter, $wheres);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function domain()
    {
        return $this->hasOne('Dendev\Leodel\Models\Domain', 'id_domaine', 'id_domaine');
    }

    public function clearance()
    {
        return $this->hasOne('Dendev\Leodel\Models\Clearance', 'id_habilitation', 'id_habilitation');
    }

    public function lessons()
    {
        return $this->hasMany('Dendev\Leodel\Models\Lesson', 'id_orientation', 'id_orientation');
    }

    public function departments()
    {
        return $this->belongsToMany('Dendev\Leodel\Models\Department', 'orientations_has_departements', 'orientations', 'departements');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    /**
     * Ajoute au model l'attribut id
     * @return mixed id du model
     */
    public function getIdAttribute()
    {
        return $this->id_orientation;
    }

    /**
     * Ajoute au model un attribut key ( à destination de liste UI )
     * @return string id du model
     */
    public function getKeyAttribute()
    {
        return $this->id;
    }

    /**
     * Ajoute au model un attribut label pour l'affichage
     * @return string label
     */
    public function getLabelAttribute()
    {
        $implantation_code = $this->implantation->code;
        return $implantation_code . '-' . $this->libelle;
    }

    /**
     * Ajoute au model un attribut value ( à destination de liste UI )
     * @return string la valeur affichable
     */
    public function getValueAttribute()
    {
        return $this->libelle;
    }


    public function  getLabelWithCodeAttribute()
    {
        return $this->libelle . ' - ' . $this->code;
    }

    public function  getCodeWithLabelAttribute()
    {
        return $this->code . ' - ' . $this->libelle;
    }

    public function  getCodeWithLabelAndImplantationAttribute()
    {
        return $this->code . ' - ' . $this->nom_implantation . ' - ' . $this->implantation->code;
    }

    public function getImplantationLabelAttribute()
    {
        return $this->implantation->nom_implantation;
    }

    public function  getLabelFullAttribute()
    {
        return $this->label . ' à ' . $this->implantation_label;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    // --
    private function _add_active_filter($rqs, $filter)
    {
        $academic_year = \AcademicYearManager::current();
        if ($filter == 'active')
            $rqs->where('annee_academique', '=', $academic_year);
        else if ($filter == 'inactive')
            $rqs->where('annee_academique', '<', $academic_year);

        return $rqs;
    }

    private function _set_lesson_identity($datas, $type)
    {
        $typed = $datas->map(function ($item, $key) use ( $type ){
            return $item->identity = $type;
        });

        return $datas;
    }
}


