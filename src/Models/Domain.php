<?php

namespace Dendev\Leodel\Models;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $connection = 'sheldon';
    protected $table = 'domaines';
    protected $primaryKey = 'id_domaine';
    protected $appends = ['id', 'key', 'value', 'label'];
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getIdAttribute()
    {
        return $this->id_domain;
    }

    public function getKeyAttribute()
    {
        return $this->id;
    }

    public function getValueAttribute()
    {
        return $this->libelle;
    }

    public function getLabelAttribute()
    {
        return $this->libelle;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
