<?php

namespace Dendev\Leodel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Implantation extends Model
{
    //use SoftDeletes;
    //use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $connection = 'sheldon';
    protected $table = 'implantations';
    protected $primaryKey = 'id_implantation';
    // public $timestamps = false;
    protected $guarded = ['id_implantation'];
    protected $appends = ['id', 'key', 'value', 'label'];
    protected $fillable = [];
    // protected $hidden = [];
    protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function get_orientations()
    {
        $orientations = DB::connection('sheldon')
            ->table('orientations_has_departements')
            ->join('orientations', 'orientations.id_orientation', '=', 'orientations_has_departements.orientations')
            ->join('departements', 'departements.id_departement', '=', 'orientations_has_departements.departements')
            ->join('implantations', 'implantations.id_implantation', '=', 'departements.id_implantation')
            ->select('orientations.*')
            ->where('implantations.id_implantation', $this->id_implantation)
            ->get()
            ->unique('id_orientation')
            ->toArray();

        return Orientation::hydrate($orientations);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function departments()
    {
        return $this->hasMany('Dendev\Leodel\Models\Department', 'id_implantation', 'id_implantation');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getIdAttribute()
    {
        return $this->id_implantation;
    }

    public function getKeyAttribute()
    {
        return $this->id;
    }

    public function getValueAttribute()
    {
        return $this->nom_site;
    }

    public function getLabelAttribute()
    {
        return $this->nom_site;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
