<?php

namespace Dendev\Leodel\Models;

use Illuminate\Database\Eloquent\Model;

class Clearance extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $connection = 'sheldon';
    protected $table = 'habilitations';
    protected $primaryKey = 'id_habilitation';
    protected $appends = ['id', 'key', 'value', 'label'];
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getIdAttribute()
    {
        return $this->id_habilitation;
    }

    public function getKeyAttribute()
    {
        return $this->id;
    }

    public function getValueAttribute()
    {
        return $this->libelle_etude;
    }

    public function getLabelAttribute()
    {
        return $this->libelle_etude;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
