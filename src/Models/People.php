<?php

namespace Dendev\Leodel\Models;

use Illuminate\Database\Eloquent\Model;
use Dendev\Leodel\Events\PeopleRetrieved;
use Illuminate\Support\Facades\DB;
use Dendev\Leodel\Traits\UtilModel;

/**
 * Offre une multitude de methodes pour récupérer des données relatives au people.
 *
 * La plus part des methodes sont des interfaces user-friendly permettant d'obtenir rapidement les données désirées. Elles font elles mm appel à des methodes plus générique.
 *
 * Les methodes générique permettent plus de liberté mais demandes plus de compréhension.
 *
 * Un filtre active et extras et souvent présent.
 * Le premier permet de ne récupérer que les données active, inactive, toutes.
 * Extras permet de construire son propre filtre de selection de résultats.
 *
 * Class People
 * @package Dendev\Leodel\Models
 */
class People extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    use UtilModel;

    protected $connection = 'sheldon';
    protected $table = 'people';
    protected $primaryKey = 'id_people';
    //public $incrementing = true;
    // public $timestamps = false;
    protected $guarded = ['id_people'];
    protected $fillable = [];
    // protected $hidden = [];
    protected $dates = ['created_at', 'updated_at'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    // Find

    // // people
    public function get_by_email($personal = false)
    {
        // TODO
        $rqs = DB::connection('sheldon')
            ->table($table)
            ->join('implantations', 'implantations.numero', '=', "$table.numero")
            ->join('departements', 'departements.id_implantation', '=', "implantations.id_implantation")
            ->select('departements.*')
            ->where("$table.id_people", $this->id);
    }

    // Get
    //  // personas
    /**
     * Retourne tous les personas  possible du people
     *
     * Persona étant les roles/identité que peux avoir le people ( teacher, student, external, administrative )
     *
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @param bool $distinct retourne un resultat avec persona distinct ou tout en un
     * @return Collection liste des des personnas
     * @throws \Exception
     */
    public function get_personas( $filter = 'active', $extras = [], $distinct = true)
    {
        $personas = collect();

        $authorized_personas = $this->_get_authorized_personas();
        foreach ($authorized_personas as $persona)
        {
            $personas_found = $this->get_personas_specific($persona, $filter, $extras);
            if ($distinct)
                $personas->put($persona, $personas_found);
            else
                $personas = $personas->merge($personas_found)->unique('id_people');
        }


        return $personas;
    }

    /**
     * Retourne les personas Administrative du people si celui ci existe
     *
     * Les personas possible sont student, teacher, administrative, external
     *
     * @param string $filter active|inactive|all
     * @return Collection liste de models administrative
     * @throws \Exception Lors de l'utilisation d'un persona inexistant
     */
    public function get_personas_administrative($filter = 'active')
    {
        return $this->get_personas_specific('administrative', $filter);
    }

    /**
     * Retourne les personas External du people si celui ci existe
     *
     * Les personas possible sont student, teacher, administrative, external
     *
     * @param string $filter active|inactive|all
     * @return Collection liste de models external
     * @throws \Exception Lors de l'utilisation d'un persona inexistant
     */
    public function get_personas_external($filter = 'active')
    {
        return $this->get_personas_specific('external', $filter);
    }

    /**
     * Retourne les personas du people du type passé en argument
     *
     * Persona étant les roles/identité que peux avoir le people ( teacher, student, external, administrative )
     *
     * @param string $persona student, teacher, administrative, external
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return Collection  liste des des personnas
     * @throws \Exception
     */
    public function get_personas_specific($persona, $filter = 'active', $extras = [])
    {
        if ( $this->_is_authorized_persona( $persona ) )
        {
            $classname = $this->_get_persona_to_classname($persona);
            $table = $this->_get_persona_to_table($persona);

            // basic rqs
            $rqs = $this->hasMany($classname, 'id_people', 'id_people');

            // specials needs
            if( $persona == 'student' )
                $rqs = $rqs->where('email_ordre', 2);

            // add filters
            $rqs = $this->_add_active_filter($rqs, $persona, $filter, $table);

            // add extra filters
            $rqs = $this->_add_extra_filters($rqs, $extras); // FIXME

            // debug
            // dd( $rqs->toSql() );

            // execute rqs
            $personas = $rqs->get()->toArray();
        }
        else
        {
            throw new \Exception(__METHOD__ . "# $persona n'est pas un persona utilisable. Les personas possible sont student, administrative, teacher, external");
        }

        return $classname::hydrate($personas);
    }

    /**
     * Retourne les personas Student du people si celui ci existe
     *
     * Les personas possible sont student, teacher, administrative, external
     *
     * @param string $filter active|inactive|all
     * @return Collection de models Student
     * @throws \Exception Lors de l'utilisation d'un persona inexistant
     */
    public function get_personas_student($filter = 'active')
    {
        return $this->get_personas_specific('student', $filter);
    }

    /**
     * Retourne les personas Teacher du people si celui ci existe
     *
     * Les personas possible sont student, teacher, administrative, external
     *
     * @param string $filter active|inactive|all
     * @return Collection liste de models Teacher
     * @throws \Exception Lors de l'utilisation d'un persona inexistant
     */
    public function get_personas_teacher($filter = 'active')
    {
        return $this->get_personas_specific('teacher', $filter);
    }

    // //  departments
    /**
     * Renvoi la liste des departements auxquelles la personne est rattaché.
     *
     * Elle est rataché par le role qu'elle y joue ( persona : student, teacher, external, administrative )
     *
     * @param string $filter Par défaut la liste ne renvoi que les implantations ou la personne est active
     * @param bool $distinct si true alors retrouve une liste distinquant les personnas [ [student] => [implantations...]]
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return Collection liste des departements du people
     */
    public function get_departments($filter = 'active', $distinct = true, $extras = [])
    {
        return $this->_get_resources('department', $filter, $distinct, $extras);
    }

    /**
     * Retourne la liste des departments ou le people est administratif
     *
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @return \Illuminate\Support\Collection|null
     */
    public function get_departments_administrative($filter = 'active')
    {
        return $this->get_departments_of_persona('administrative', $filter);
    }

    /**
     * Retourne la liste des departments ou le people est étudiant
     *
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @return \Illuminate\Support\Collection|null
     */
    public function get_departments_external($filter = 'active')
    {
        return $this->get_departments_of_persona('external', $filter);
    }

    /**
     * Retourne la liste des departements pour le role ( persona ) de People
     *
     * Un people peut appartenir à différentes implantations et y tenir des roles différents.
     * Pour éviter la confusion entre roles au sens de permissions et le role la fonction tenue dans une implantation, le terme persona est utilisé
     *
     * Seule les implantations ou le people est actif sont reprise.
     *
     * @param $persona les valeurs possible sont "teacher", "student", "external", "administrative"
     * @param $filter active, inactive, all limite la selection retourné
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return \Illuminate\Support\Collection|null  Collection de models Implantation
     */
    public function get_departments_of_persona($persona, $filter = 'active', $extras = [])
    {
        $departments = [];

        // check arg persona
        if( $this->_is_authorized_persona($persona) )
        {
            // use correct table
            $table = $this->_get_persona_to_table( $persona );

            // make basic rqs
            $rqs = DB::connection('sheldon')
                ->table($table)
                ->join('implantations', 'implantations.numero', '=', "$table.numero")
                ->join('departements', 'departements.id_implantation', '=', "implantations.id_implantation")
                ->select('departements.*')
                ->where("$table.id_people", $this->id);

            // add filters
            $rqs = $this->_add_active_filter($rqs, $persona, $filter, $table);

            // add extra filters
            $rqs = $this->_add_extra_filters($rqs, $extras);

            // debug
            // dd( $rqs->toSql());

            // execute rqs
            $departments = $rqs->get()->unique('id_departement')->toArray();
        }

        return Department::hydrate($departments);
    }

    /**
     * Retourne la liste des departments ou le people est étudiant
     *
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @return \Illuminate\Support\Collection|null
     */
    public function get_departments_student($filter = 'active')
    {
        return $this->get_departments_of_persona('student', $filter);
    }

    /**
     * Retourne la liste des departments ou le people est enseignant
     *
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @return \Illuminate\Support\Collection|null
     */
    public function get_departments_teacher($filter = 'active')
    {
        return $this->get_departments_of_persona('teacher', $filter);
    }

    // Domains
    /**
     * Fournit la liste des domains du people
     *
     * @param $filter active, inactive, all limite la selection retourné
     * @param bool $distinct si true alors retrouve une liste distinquant les personnas [ [student] => [implantations...]]
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return Collection liste des domaines
     * @throws \Exception emit par la mth core
     */
    public function get_domains($filter = 'active', $distinct = true, $extras = [])
    {
        return $this->_get_resources('domain', $filter, $distinct, $extras);
    }

    /**
     *  Fournit la liste des domains du people selon son persona
     *
     * @param $persona
     * @param $filter active, inactive, all limite la selection retourné
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return Collection liste des domaines
     * @throws \Exception emit par la mth core
     */
    public function get_domains_of_persona($persona, $filter = 'active', $extras = [])
    {
        return $this->_get_resources_by_persona('domain', $persona, $filter, $extras);
    }

    // Implantations
    /**
     * Renvoi la liste des implantations auxquelles la personne est rattaché.
     *
     * Elle est rataché par le role qu'elle y joue ( persona : student, teacher, external, administrative )
     *
     * @param string $filter Par défaut la liste ne renvoi que les implantations ou la personne est active
     * @param bool $distinct si true alors retrouve une liste distinquant les personnas [ [student] => [implantations...]]
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return Collection liste des implantations du people, tous personas confondus
     */
    public function get_implantations($filter = 'active', $distinct = true, $extras = [])
    {
        return $this->_get_resources('implantation', $filter, $distinct, $extras);
    }

    /**
     * Retourne la liste des implantations ou le people est administratif
     *
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return Collection liste des implantations ou le people est administrative
     */
    public function get_implantations_administrative($filter = 'active', $extras = [])
    {
        return $this->get_implantations_of_persona('administrative', $filter, $extras);
    }

    /**
     * Retourne la liste des implantations ou le people est étudiant
     *
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return Collection liste des implantations ou le people est externe
     */
    public function get_implantations_external($filter = 'active', $extras = [])
    {
        return $this->get_implantations_of_persona('external', $filter, $extras);
    }

    /**
     * Retourne la liste des implantations pour le role ( persona ) de People
     *
     * Un people peut appartenir à différentes implantations et y tenir des roles différents.
     * Pour éviter la confusion entre roles au sens de permissions et le role la fonction tenue dans une implantation, le terme persona est utilisé
     *
     * Seule les implantations ou le people est actif sont reprise.
     *
     * @param $persona les valeurs possible sont "teacher", "student", "external", "administrative"
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return \Illuminate\Support\Collection|null  Collection de models Implantation
     */
    public function get_implantations_of_persona($persona, $filter = 'active', $extras = []) // TODO add extras filters  [colo => ['cond' => value]]
    {
        $implantations = [];

        // check arg persona
        if( $this->_is_authorized_persona($persona) )
        {
            // use correct table
            $table = $this->_get_persona_to_table( $persona );

            // make basic rqs
            $rqs = DB::connection('sheldon')
                ->table($table)
                ->join('implantations', 'implantations.numero', '=', "$table.numero")
                ->select('implantations.*')
                ->where("$table.id_people", $this->id);

            // add filters
            $rqs = $this->_add_active_filter($rqs, $persona, $filter, $table);

            // add extra filters
            $rqs = $this->_add_extra_filters($rqs, $extras);

            // debug
            // dd( $rqs->toSql());

            // execute rqs
            $implantations = $rqs->get()->unique('id_implantation')->toArray();
        }
        else
        {
            throw new \Exception("$persona n'est pas un persona utilisable. Les personas possible sont student, administrative, teacher, external");
        }

        return Implantation::hydrate($implantations);
    }



    /**
     * Retourne la liste des implantations ou le people est étudiant
     *
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return Collection liste des implantations ou le people est student
     */
    public function get_implantations_student($filter = 'active', $extras = [])
    {
        return $this->get_implantations_of_persona('student', $filter, $extras);
    }

    /**
     * Retourne la liste des implantations ou le people est enseignant
     *
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return Collection liste des implantations ou le people est teacher
     */
    public function get_implantations_teacher($filter = 'active', $extras = [])
    {
        return $this->get_implantations_of_persona('teacher', $filter, $extras);
    }


    // Sectors
    /**
     * Fournit la liste des secteurs du people
     *
     * @param $filter active, inactive, all limite la selection retourné
     * @param bool $distinct si true alors retrouve une liste distinquant les personnas [ [student] => [implantations...]]
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return Collection liste des secteurs
     * @throws \Exception emit par la mth core
     */
    public function get_sectors($filter = 'active', $distinct = true, $extras = [])
    {
        return $this->_get_resources('sector', $filter, $distinct, $extras);
    }

    /**
     *  Fournit la liste des secteurs du people selon son persona
     *
     * @param $persona
     * @param $filter active, inactive, all limite la selection retourné
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return Collection liste des secteurs
     * @throws \Exception
     */
    public function get_sectors_of_persona($persona, $filter = 'active', $extras = [])
    {
        return $this->_get_resources_by_persona('sector', $persona, $filter, $extras);
    }

    // Orientations
    /**
     * Retourne la liste des orientations du people sous forme de personas distinct ou de liste indistincte
     *
     * @param string $filter active, inactive, all limite la selection retourné
     * @param bool $distinct retourner un tableau des ressouces classé par personas ( distinct == true ) ou une liste indistincte ( distinct == false )
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return Collection liste des orientations du people
     */
    public function get_orientations($filter = 'active', $distinct = true, $extras = [])
    {
        return $this->_get_resources('orientation', $filter, $distinct, $extras);
    }

    /**
     * Retourne la liste des orientations où le people est administratif
     *
     * @param $filter active, inactive, all limite la selection retourné
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return \Illuminate\Support\Collection|null
     */
    public function get_orientations_administrative($filter = 'active', $extras = [])
    {
        return $this->get_orientations_of_persona('administrative', $filter, $extras);
    }

    /**
     * Retourne la liste des orientations où le people est externe
     *
     * @param $filter active, inactive, all limite la selection retourné
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return \Illuminate\Support\Collection|null
     */
    public function get_orientations_external($filter = 'active', $extras = [])
    {
        return $this->get_orientations_of_persona('external', $filter, $extras);
    }

    /**
     * Retourne les orientations du people d'après son persona
     * @param string $persona les valeurs possible sont "teacher", "student", "external", "administrative"
     * @param string $filter active, inactive, all limite la selection retourné
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return mixed liste des orientations du people selon son persona
     */
    public function get_orientations_of_persona($persona, $filter = 'active', $extras = [])
    {
        return $this->_get_resources_by_persona('orientation', $persona, $filter, $extras);
    }

    /**
     * Retourne la liste des orientations où le people est étudiant
     *
     * @param $filter active, inactive, all limite la selection retourné
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return \Illuminate\Support\Collection|null
     */
    public function get_orientations_student($filter = 'active', $extras = [])
    {
        return $this->get_orientations_of_persona('student', $filter, $extras);
    }

    /**
     * Retourne la liste des orientations où le people est enseignant
     *
     * @param $filter active, inactive, all limite la selection retourné
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return \Illuminate\Support\Collection|null
     */
    public function get_orientations_teacher($filter = 'active', $extras = [])
    {
        return $this->get_orientations_of_persona('teacher', $filter, $extras);
    }

    // Attributions
    // TODO by person && get specific
    /**
     * Retourne la liste des attributions du people
     *
     * @return Collection liste des attributions du people
     */
    public function get_attributions($persona, $filter = 'active', $extras = []) // specific // FIXME active, extras filter ??
    {
        $departments = collect();

        $abbreviation = $this->abbreviation;

        // make basic rqs
        $rqs = Attribution::where('abreviation', $abbreviation);

        // use correct table
        $table = $this->_get_persona_to_table( $persona );

        // add filters
        $rqs = $this->_add_active_filter($rqs, $persona, $filter, $table);

        // add extra filters
        $rqs = $this->_add_extra_filters($rqs, $extras);

        // debug
        // dd( $rqs->toSql());

        // execute rqs
        $resources = $rqs->get()->unique('abreviation')->toArray();

        return $resources;
    }

    // Is
    //  // persona
    /**
     * Indique si People est un administratif ou pas
     *
     * @return bool est ou n'est pas un administratif
     */
    public function is_administrative(): bool
    {
        return $this->is_persona("administrative");
    }

    /**
     * Indique si le people est administratif dans une implantation
     *
     * @param bool $id_or_number si false peut importe l'implantation sinon se base sur l'id ou numero d'implantation fournit
     * @param string $filter ne renvoi que les implantations où le people est active, inactive, all
     * @param string $type indique si l'argument 1 est un id d'implantation ou numéro d'implantation
     * @return bool true si le people à un persona student toutes implantations confondus
     */
    public function is_administrative_implantation($id_or_number = false, $filter = 'active', $type = 'id')
    {
        return $this->is_persona_implantation('administrative', $id_or_number, $filter, $type );
    }

    /**
     * Indique si People est un externe ou pas
     *
     * @return bool est ou n'est pas un externe
     */
    public function is_external(): bool
    {
        return $this->is_persona("external");
    }

    /**
     * Indique si le people est externe dans une implantation
     *
     * @param bool $id_or_number si false peut importe l'implantation sinon se base sur l'id ou numero d'implantation fournit
     * @param string $filter ne renvoi que les implantations où le people est active, inactive, all
     * @param string $type indique si l'argument 1 est un id d'implantation ou numéro d'implantation
     * @return bool true si le people à un persona student toutes implantations confondus
     */
    public function is_external_implantation($id_or_number = false, $filter = 'active', $type = 'id')
    {
        return $this->is_persona_implantation('external', $id_or_number, $filter, $type );
    }

    /**
     * Indique si People à le persona donner en argument
     *
     * Les personas possible sont "teacher", "student", "administrative", "external"
     *
     * @return bool
     */
    public function is_persona($persona): bool
    {
        $is = false;

        if (in_array($persona, $this->personas))
        {
            $is = true;
        }

        return $is;
    }

    /**
     * Indique si le people est membre d'une implantation
     *
     * @param string $persona student, teacher, administrative, external
     * @param bool $id_or_number si false peut importe l'implantation sinon se base sur l'id ou numero d'implantation fournit
     * @param string $filter ne renvoi que les implantations où le people est active, inactive, all
     * @param string $type indique si l'argument 1 est un id d'implantation ou numéro d'implantation
     * @return bool true si le people à le persona indiqué, toutes implantations confondus
     */
    public function is_persona_implantation($persona, $id_or_number = false, $filter = 'active', $type = 'id')
    {
        if( $id_or_number )
        {
            if( $type == 'id' )
            {
                $extras = [['column' => 'id_implantation', 'operator' => '=', 'value' => $id_or_number]];
            }
            else
            {
                $extras = [['column' => 'implantations.numero', 'operator' => '=', 'value' => $id_or_number]];
            }
        }
        else
        {
            $extras = [];
        }

        $implantations = $this->get_implantations_of_persona($persona, $filter, $extras);


        return ($implantations->count() > 0 ) ? true : false;
    }

    /**
     * Indique si People est un étudiant ou pas
     *
     * @return bool est ou n'est pas un étudiant
     */
    public function is_student(): bool
    {
        return $this->is_persona("student");
    }

    /**
     * Indique si le people est etudiant dans une implantation
     *
     * @param bool $id_or_number si false peut importe l'implantation sinon se base sur l'id ou numero d'implantation fournit
     * @param string $filter ne renvoi que les implantations où le people est active, inactive, all
     * @param string $type indique si l'argument 1 est un id d'implantation ou numéro d'implantation
     * @return bool true si le people à un persona student toutes implantations confondus
     */
    public function is_student_implantation($id_or_number = false, $filter = 'active', $type = 'id')
    {
        return $this->is_persona_implantation('student', $id_or_number, $filter, $type );
    }

    /**
     * Indique si People est un enseignant ou pas
     *
     * @return bool est ou n'est pas un enseignant
     */
    public function is_teacher(): bool
    {
        return $this->is_persona("teacher");
    }

    /**
     * Indique si le people est enseignant dans une implantation
     *
     * @param bool $id_or_number si false peut importe l'implantation sinon se base sur l'id ou numero d'implantation fournit
     * @param string $filter ne renvoi que les implantations où le people est active, inactive, all
     * @param string $type indique si l'argument 1 est un id d'implantation ou numéro d'implantation
     * @return bool true si le people à un persona teacher toutes implantations confondus
     */
    public function is_teacher_implantation($id_or_number = false, $filter = 'active', $type = 'id')
    {
        return $this->is_persona_implantation('teacher', $id_or_number, $filter, $type );
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */


    /**
     * Relations avec les personas administrative du people
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function administratives()
    {
        return $this->hasMany('Dendev\Leodel\Models\Administrative', 'id_people', 'id_people');
    }

    /**
     * Relations avec les personas external du people
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function externals()
    {
        return $this->hasMany('Dendev\Leodel\Models\External', 'id_people', 'id_people');
    }

    /**
     * Relations avec les personas student du people
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function students()
    {
        return $this->hasMany('Dendev\Leodel\Models\Student', 'id_people', 'id_people');
    }

    /**
     * Relations avec les personas teacher du people
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function teachers()
    {
        return $this->hasMany('Dendev\Leodel\Models\Teacher', 'id_people', 'id_people');
    }

    /**
     * Relation entre user et people
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_people', 'people_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /**
     * Fournit l'abréviation sous forme d'attribut
     *
     * Les abréviations sont en lien avec les attributions. Un étudiant n'en a pas.
     *
     * @return bool|string False si aucune abbréviation de trouvé sinon l'abréviation est renvoyé
     */
    public function getAbbreviationAttribute()
    {
        $abbreviation = false;

        if (!$this->is_student()) {
            $name = strtoupper(str_replace(' ', '', $this->nom));
            $firstname = strtoupper(str_replace(' ', '', $this->prenom));

            $abbreviation = substr($name, 0, 3) . substr($firstname, 0, 2);
        }

        return $abbreviation;
    }

    /**
     * Ajoute au model l'attribut id
     * @return string id du model
     */
    public function getIdAttribute()
    {
        return $this->id_people;
    }

    /**
     * Ajoute au model un attribut key ( à destination de liste UI )
     * @return string id du model
     */
    public function getKeyAttribute()
    {
        return $this->id;
    }

    /**
     * Ajoute au model un attribut label pour l'affichage
     * @return string label
     */
    public function getLabelAttribute()
    {
        return $this->label;
    }

    /**
     * Ajoute au model un attribut value ( à destination de liste UI )
     * @return string la valeur affichable
     */
    public function getValueAttribute()
    {
        return $this->nom . ' ' . $this->prenom;
    }

    public function getNumproecoAttribute()
    {
        return $this->matricule;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    /**
     * Ajouter la liste des personas du people
     * @param $personas
     */
    public function setPersonasAttribute($personas)
    {
        $this->attributes['personas'] = $personas;
    }

    /*
    |--------------------------------------------------------------------------
    | EVENTS
    |--------------------------------------------------------------------------
    */
    /**
     * Gestions des events sur le model poeple
     * @var array
     */
    protected $dispatchesEvents = [
        'retrieved' => PeopleRetrieved::class
    ];

    /*
    |--------------------------------------------------------------------------
    | PRIVATE
    |--------------------------------------------------------------------------
    */
    //  // utils
    /**
     * Ajoute un filtre sur la request sql afin de limiter la selection pour prendre les données active ou inactive
     *
     * @param Builder $rqs request sql laravel
     * @param string $persona les valeurs possible sont "teacher", "student", "external", "administrative"
     * @param string $filter active, inactive, all limite la selection retourné
     * @param string $table nom de la table devant être utiliser
     * @return Builder request sql avec un filter where en plus
     */
    private function _add_active_filter($rqs, $persona, $filter, $table)
    {
        // add filters
        if ($persona == 'student')
        {
            if ($filter == 'active')
            {
                // active student
                $academic_year = \AcademicYearManager::current();
                $rqs = $rqs->where("$table.annee_academique", $academic_year);
            }
            else if ($filter == 'inactive')
            {
                // inactive student
                $academic_year = \AcademicYearManager::current();
                $rqs = $rqs->where("$table.annee_academique", '<', $academic_year);
            }
        }
        else
        {
            if ($filter == 'active')
            {
                // active administratives, teacher, external
                $rqs = $rqs->where("$table.presence_physique", true);
                $rqs = $rqs->where(function ($query) use ( $table ){ // make an WHERE ( cond1 OR cond 2 ) AND ...
                    $query->whereNull("$table.date_fin")->orWhereDate("$table.date_fin", '>=', now());
                });
            }
            else if ($filter == 'inactive')
            {
                // inactive administratives, teacher, external
                $rqs = $rqs->where(function ($query) use ( $table ){ // make an WHERE ( cond1 OR cond 2 ) AND ...
                    $query->whereNull("$table.date_fin")->orWhereDate("$table.date_fin", '<', now());
                });
            }
        }

        return $rqs;
    }

    //  // refs
    /**
     * Vérifie que le persona est utilisable
     *
     * Liste des personas possible: teacher, student, external, administrative
     *
     * @param string $persona persona devant être vérifier
     * @return bool False si le persona n'existe pas
     */
    private function _is_authorized_persona($persona)
    {
        $is_authorized = false;

        $authorized_personas = [
            'teacher',
            'student',
            'external',
            'administrative',
        ];

        if( in_array($persona, $authorized_personas ) )
            $is_authorized = true;

        return $is_authorized;
    }

    /**
     * Transforme un persona en sa table sheldon
     * @param $persona nom du persona
     * @return bool|string False si persona n'existe pas. Sinon le nom de la table
     */
    private function _get_persona_to_table($persona)
    {
        $table = false;

        $authorized_persona_to_tables = [
            'teacher' => 'enseignants',
            'student' => 'etudiants',
            'external' => 'externes_he',
            'administrative' => 'administratifs'
        ];

        if (array_key_exists($persona, $authorized_persona_to_tables))
            $table = $authorized_persona_to_tables[$persona];

        return $table;
    }

    /**
     * Transforme un persona en sa class
     *
     * @param $persona nom du persona
     * @return bool|string False si persona n'existe pas, sinon le classname
     */
    private function _get_persona_to_classname($persona)
    {
        $model = false;

        $authorized_persona_to_model = [
            'teacher' => '\Dendev\Leodel\Models\Teacher',
            'student' => '\Dendev\Leodel\Models\Student',
            'external' => '\Dendev\Leodel\Models\External',
            'administrative' => '\Dendev\Leodel\Models\Administrative'
        ];

        if (array_key_exists($persona, $authorized_persona_to_model))
            $model = $authorized_persona_to_model[$persona];

        return $model;
    }

    /**
     * fournit la liste des personas valide
     * @return array liste des personas
     */
    private function _get_authorized_personas()
    {
        return ['teacher', 'student', 'external', 'administrative'];
    }

    //  // rqs
    /**
     * Créé une request sql de base pour la récupération des implantations d'après un persona
     *
     * @param string $id id du people
     * @param string $persona persona du people
     * @param string $table table devant être utilise ( enseignants, etudiants, externes, administratifs )
     * @return \Illuminate\Database\Query\Builder liste des implantations
     */
    private function _make_rqs_implantations_by_persona($id, $persona, $table)
    {
        $rqs = DB::connection('sheldon')
            ->table($table)
            ->join('implantations', 'implantations.numero', '=', "$table.numero")
            ->select('implantations.*')
            ->where("$table.id_people", $this->id);
        return $rqs;
    }
    /**
     * Créé une request sql de base pour la récupération des departements d'après un persona
     *
     * @param string $id id du people
     * @param string $persona persona du people
     * @param string $table table devant être utilise ( enseignants, etudiants, externes, administratifs )
     * @return \Illuminate\Database\Query\Builder liste des departements
     */
    private function _make_rqs_departments_by_persona($id, $persona, $table)
    {
        $rqs = DB::connection('sheldon')
            ->table($table)
            ->join('implantations', 'implantations.numero', '=', "$table.numero")
            ->join('departements', 'departements.id_implantation', '=', "implantations.id_implantation")
            ->select('departements.*')
            ->where("$table.id_people", $this->id);
        return $rqs;
    }

    /**
     * Choisit quelle requete doit être crée pour la récupération des orientations d'après un persona
     *
     * Student ne fonctionne pas en db comme les autres
     *
     * @param string $id id du people
     * @param string $persona persona du people
     * @param string $table table devant être utilise ( enseignants, etudiants, externes, administratifs )
     * @return \Illuminate\Database\Query\Builder liste des orientations
     */
    private function _make_rqs_orientations_by_persona($id, $persona, $table)
    {
        $rqs = false;

        if( $persona == 'student' )
            $rqs = $this->_make_rqs_student_orientations($id);
        else
            $rqs = $this->_make_rqs_others_persona_orientations($table, $id);

        return $rqs;
    }

    /**
     * Choisit quelle requete doit être crée pour la récupération des domaines d'après un persona
     *
     * Student ne fonctionne pas en db comme les autres
     *
     * @param string $id id du people
     * @param string $persona persona du people
     * @param string $table table devant être utilise ( enseignants, etudiants, externes, administratifs )
     * @return \Illuminate\Database\Query\Builder liste des domaines
     */
    private function _make_rqs_domains_by_persona($id, $persona, $table)
    {
        $rqs = false;

        if( $persona == 'student' )
            $rqs = $this->_make_rqs_student_domains($id);
        else
            $rqs = $this->_make_rqs_others_persona_domains($table, $id);

        return $rqs;
    }

    /**
     * Choisit quelle requete doit être crée pour la récupération des secteurs d'après un persona
     *
     * Student ne fonctionne pas en db comme les autres
     *
     * @param string $id id du people
     * @param string $persona persona du people
     * @param string $table table devant être utilise ( enseignants, etudiants, externes, administratifs )
     * @return \Illuminate\Database\Query\Builder liste des secteurs
     */
    private function _make_rqs_sectors_by_persona($id, $persona, $table)
    {
        $rqs = false;

        if( $persona == 'student' )
            $rqs = $this->_make_rqs_student_sectors($id);
        else
            $rqs = $this->_make_rqs_others_persona_sectors($table, $id);

        return $rqs;
    }

    /**
     * Création de la request sql pour selectionner les orientations du persona student du people
     * @param string $id id du people
     * @return \Illuminate\Database\Query\Builder request de selection des orientations
     */
    private function _make_rqs_student_orientations($id)
    {
        return $this->_make_rqs_student($id, 'orientations.*');
    }

    /**
     * Création de la request sql pour selectionner les domaines du persona student du people
     * @param string $id id du people
     * @return \Illuminate\Database\Query\Builder request de selection des domains
     */
    private function _make_rqs_student_domains($id)
    {
        return $this->_make_rqs_student($id, 'domaines.*');
    }

    /**
     * Création de la request sql pour selectionner les secteurs du persona student du people
     * @param string $id id du people
     * @return \Illuminate\Database\Query\Builder request de selection des secteurs
     */
    private function _make_rqs_student_sectors($id)
    {
        return $this->_make_rqs_student($id, 'secteurs.*');
    }

    /**
     * Création de la request général de récupération de données en rapport avec le persona student
     * @param $id id du people
     * @param $select selection des données à récupérer
     * @return \Illuminate\Database\Query\Builder request sql
     */
    private function _make_rqs_student($id, $select)
    {
        $rqs = DB::connection('sheldon')
            ->table('etudiants')
            ->join('etudiants_many_cours', 'etudiants_many_cours.id_etudiant', '=', "etudiants.id_etudiant")
            ->join('cours', 'cours.id_cours', '=', "etudiants_many_cours.id_cours")
            ->join('orientations', 'orientations.id_orientation', '=', "cours.id_orientation")
            ->join('domaines', 'domaines.id_domaine', '=', "orientations.id_domaine")
            ->join('secteurs', 'secteurs.id_secteur', '=', "domaines.id_secteur")
            ->select($select)
            ->where("etudiants.id_people", $id);


        return $rqs;
    }

    /**
     * Création de la request sql pour selectionner les orientations des personas non student
     * @param string $table table en db à utiliser
     * @param string $id id du people
     * @return \Illuminate\Database\Query\Builder request de selection des orientations
     */
    private function _make_rqs_others_persona_orientations($table, $id)
    {
        return $this->_make_rqs_others_persona($table, $id, 'orientations.*');
    }

    /**
     * Création de la request sql pour selectionner les domaines des personas non student
     * @param string $table table en db à utiliser
     * @param string $id id du people
     * @return \Illuminate\Database\Query\Builder request de selection des secteurs
     */
    private function _make_rqs_others_persona_domains($table, $id)
    {
        return $this->_make_rqs_others_persona($table, $id, 'domaines.*');
    }

    /**
     * Création de la request sql pour selectionner les secteurs des personas non student
     * @param string $table table en db à utiliser
     * @param string $id id du people
     * @return \Illuminate\Database\Query\Builder request de selection des secteurs
     */
    private function _make_rqs_others_persona_sectors($table, $id)
    {
        return $this->_make_rqs_others_persona($table, $id, 'secteurs.*');
    }

    /**
     * Création de la request général de récupération de données en rapport avec personas non student
     * @param string table en db à utiliser
     * @param string $id id du people
     * @param string $select selection des données à récupérer
     * @return \Illuminate\Database\Query\Builder request sql
     */
    private function _make_rqs_others_persona($table, $id, $select)
    {
        $rqs = DB::connection('sheldon')
            ->table($table)
            ->join('attributions', 'attributions.abreviation', '=', "$table.abreviation")
            ->join('orientations', 'orientations.code_pe', '=', "attributions.orientation")
            ->join('domaines', 'domaines.id_domaine', '=', "orientations.id_domaine")
            ->join('secteurs', 'secteurs.id_secteur', '=', "domaines.id_secteur")
            ->select($select)
            ->where("$table.id_people", $id);

        return $rqs;
    }

    //  // resources
    /**
     * Se charge de crée une liste de la ressource en lien avec l'utilisateur.
     *
     * Les ressources possible sont 'orientation', 'implantation',
     * Methode générique servant à d'autres méthodes plus précise.
     * Cette méthode n'as pas vocation à être utilisé directement.
     * Elle implemente une fonctionnalitées offerte aux autres methodes.
     *
     * @param string $resource_type le type de ressources que l'on veut obtenir
     * @param string $filter selectionner les ressources active, inactive, all
     * @param bool $distinct retourner un tableau des ressouces classé par personas ( distinct == true ) ou une liste indistincte ( distinct == false )
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return Collection liste des ressources
     * @throws \Exception Leve une erreur si le type de ressouce n'existe pas
     */
    private function _get_resources($resource_type, $filter = 'active', $distinct = true, $extras = [])
    {
        // refs
        $resources = collect();
        $resource_types = [
            'implantation' => ['mth' => 'get_implantations_of_persona', 'unique_key' => 'id_implantation'],
            'department' => ['mth' => 'get_departments_of_persona', 'unique_key' => 'id_implantation'],
            'orientation' => ['mth' => 'get_orientations_of_persona', 'unique_key' => 'id_orientation'],
            'domain' => ['mth' => 'get_domains_of_persona', 'unique_key' => 'id_domain'],
            'sector' => ['mth' => 'get_sectors_of_persona', 'unique_key' => 'id_sector'],
        ];

        // continue or quit
        if( ! array_key_exists($resource_type, $resource_types) )
            throw new \Exception(__METHOD__ . "# La ressource demandé n'existe pas ( $resource_type )");

        // get method to call
        $get_mth = $resource_types[$resource_type]['mth'];
        $unique_key = $resource_types[$resource_type]['unique_key'];

        // call method
        $authorized_personas = $this->_get_authorized_personas();
        foreach ($authorized_personas as $persona)
        {
            $resources_found = $this->$get_mth($persona, $filter, $extras);
            if ($distinct)
                $resources->put($persona, $resources_found);
            else
                $resources = $resources->merge($resources_found)->unique($unique_key);
        }

        // end
        return $resources;
    }

    /**
     * Fournit la liste de la ressource demandé en lien avec le people et pour un persona précis.
     *
     *
     * @param string $resource_type type de ressource désiré ( implantation, orientation, department, domain, sector )
     * @param string $persona le persona pour laquelle on désire la ressource ( teacher, student, external, administrative )
     * @param string $filter selectionner les ressources active, inactive, all
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return Collection liste de la ressource demandé
     * @throws \Exception Leve une erreur si argument resource_type ou persona n'est pas bon
     */
    private function _get_resources_by_persona($resource_type, $persona, $filter = 'active', $extras = [])
    {
        // refs
        $resources = [];
        $resource_types = [
            'implantation' => ['classname' => '\Dendev\Leodel\Models\Implantation', 'unique_key' => 'implantation_id', 'rqs_mth' => '_make_rqs_implantations_by_persona'],
            'orientation' => ['classname' => '\Dendev\Leodel\Models\Orientation', 'unique_key' => 'orientation_id', 'rqs_mth' => '_make_rqs_orientations_by_persona'],
            'department' => ['classname' => '\Dendev\Leodel\Models\Department', 'unique_key' => 'departement_id', 'rqs_mth' => '_make_rqs_departments_by_persona'],
            'domain' => ['classname' => '\Dendev\Leodel\Models\Domain', 'unique_key' => 'domaine_id', 'rqs_mth' => '_make_rqs_domains_by_persona'],
            'sector' => ['classname' => '\Dendev\Leodel\Models\Sector', 'unique_key' => 'secteur_id', 'rqs_mth' => '_make_rqs_sectors_by_persona'],
        ];

        // continue or quit
        if( ! array_key_exists($resource_type, $resource_types) )
            throw new \Exception(__METHOD__ . "# La ressource demandé n'existe pas ( $resource_type )");

        if( ! $this->_is_authorized_persona($persona) )
            throw new \Exception(__METHOD__. "# $persona n'est pas un persona utilisable. Les personas possible sont student, administrative, teacher, external");

        // get method to call
        $classname = $resource_types[$resource_type]['classname'];
        $unique_key = $resource_types[$resource_type]['unique_key'];
        $rqs_mth = $resource_types[$resource_type]['rqs_mth'];

        // use correct table
        $table = $this->_get_persona_to_table( $persona );

        // make basic rqs
        $rqs = $this->$rqs_mth($this->id, $persona, $table);

        // add filters
        $rqs = $this->_add_active_filter($rqs, $persona, $filter, $table);

        // add extra filters
        $rqs = $this->_add_extra_filters($rqs, $extras);

        // debug
        // dd( $rqs->toSql());

        // execute rqs
        $resources = $rqs->get()->unique($unique_key)->toArray();

        return $classname::hydrate($resources);
    }
}
