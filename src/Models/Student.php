<?php
namespace Dendev\Leodel\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //use SoftDeletes;
    //use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $connection = 'sheldon';
    protected $table = 'etudiants';
    protected $primaryKey = 'id_etudiant';
    // public $timestamps = false;
    protected $guarded = ['id_etudiant'];
    protected $fillable = [];
    // protected $hidden = [];
    protected $dates = ['date_entree', 'date_fin'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function people()
    {
        return $this->belongsTo('Dendev\Leodel\Models\People', 'id_people', 'id_people');
    }

    public function implantation()
    {
        return $this->hasOne('Dendev\Leodel\Models\Implantation', 'numero', 'numero');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User')->using('App\Models\People');
    }

    public function cours()
    {
        return $this->belongsToMany('Dendev\Leodel\Models\Lesson', 'etudiants_many_cours', 'id_etudiant', 'id_cours');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
