<?php

namespace Dendev\Leodel\Models;

use Illuminate\Database\Eloquent\Model;
use Dendev\Leodel\Traits\UtilModel;
use phpDocumentor\Reflection\Types\Collection;

class Lesson extends Model
{
    use UtilModel;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $connection = 'sheldon';
    protected $table = 'cours';
    protected $primaryKey = 'id_cours';
    protected $appends = ['id', 'key', 'value', 'label', 'label_year', 'label_class', 'label_bloc'];
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->identity = 'lesson';
    }

    /**
     * Indique si le cours est durant l'année acédémique courante
     *
     * @return bool true|false
     */
    public function is_active() : bool
    {
        $academic_year = \AcademicYearManager::current();
        return ($this->annee_academique == $academic_year) ? true : false;
    }

    /**
     * Indique si le cours est une UE
     *
     * @return bool true|false
     */
    public function is_ue() : bool
    {
        return ($this->cours_ue == '') ? false : true;
    }

    /**
     * Indique si le cours est une AA
     *
     * @return bool true|false
     */
    public function is_aa()
    {
        return ($this->cours_ue == '') ? true : false;
    }

    /**
     * Indique si le cours a des AA
     *
     * @return bool true|false
     */
    public function has_aa()
    {
        return ($this->cours_ue == '') ? false : true;
    }

    /**
     * Retourne les AA du cours
     *
     * Les AA sont des cours de type AA
     *
     * @param filter active par défaut, filtre la selection retourné ( active, inactive, all )
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return \Illuminate\Support\Collection liste des aas
     */
    public function get_aas($filter = 'active', $extras = [])
    {
        $lessons = collect();

        if ($this->is_ue() && $this->has_aa())
        {
            foreach ($this->aa_ids as $aa_id)
            {
                $rqs = Lesson::where('numproeco', $aa_id);

                // filter
                $this->_add_active_filter($rqs, $filter, 'cours');

                // extras
                $rqs = $this->_add_extra_filters($rqs, $extras);

                // datas
                $datas = $rqs->first();

                // debug
                //dd( $rqs->toSql());

                // identity
                $lesson = $this->_set_identity($datas, 'aa');

                // keep result
                if (!is_null($lesson))
                    $lessons->push($lesson);
            }
        }

        return $lessons;
    }

    /**
     * Retourne la UE du cours
     *
     * @param filter active par défaut, filtre la selection retourné ( active, inactive, all )
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return Lesson model Lesson de l'ue
     */
    public function get_ue($filter = 'active', $extras = [])
    {
        $lesson = false;

        if ($this->is_aa())
        {
            $rqs = Lesson::where('cours_ue', 'LIKE', "%{$this->numproeco}%");
            // filter
//            $this->_add_active_filter($rqs, $filter, 'cours');

            // extras
           $rqs = $this->_add_extra_filters($rqs, $extras);

            // datas
            $datas = $rqs->first();

            // debug
            //dd( $rqs->toSql());

            // identity
            $lesson = $this->_set_identity($datas, 'ue');

        }

        return $lesson;
    }

    /**
     * Retourne les étudiants liés au cours
     *
     *
     * @param filter active par défaut, filtre la selection retourné ( active, inactive, all )
     * @param array $extras tableau pour filter le resultat ex [ [column => 'statut', 'operator' => '=', 'value' => 'O' ] ]
     * @return Collection collection de Student
     */
    public function get_students( $filter = 'active', $extras = [] ) // FIXME cours de l'ue existe ou c'est cours des aa de l'ue ?
    {
        $students = false;

        $rqs = $this->students();

        // filter
        $this->_add_active_filter($rqs, $filter, 'etudiants_many_cours');

        // extras
        $rqs = $this->_add_extra_filters($rqs, $extras);

        // do rqs
        $students = $rqs->get();

        return $students;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function implantation()
    {
        return $this->hasOne('Dendev\Leodel\Models\Implantation', 'numero', 'numero');
    }

    public function orientation()
    {
        return $this->hasOne('Dendev\Leodel\Models\Orientation', 'id_orientation', 'id_orientation');
    }

    public function students()
    {
        return $this->belongsToMany('Dendev\Leodel\Models\Student', 'etudiants_many_cours', 'id_cours', 'id_etudiant');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function getIdAttribute()
    {
        return $this->id_cours;
    }

    public function getKeyAttribute()
    {
        return $this->id;
    }

    public function getValueAttribute()
    {
        if( $this->identity == 'lesson' )
            return $this->label;
        else if( $this->identity == 'bloc' )
            return $this->label_bloc;
        else if( $this->identity == 'year' )
            return $this->label_year;
        else if( $this->identity == 'class' )
            return $this->label_class;
    }

    public function getLabelAttribute()
    {
        $implantation_code = $this->implantation->code;
        return $implantation_code . '-' . $this->code_pe . '-' . $this->annee;
    }

    public function getLabelBlocAttribute()
    {
        $implantation_code = $this->implantation->code;
        $classe = ( is_null( $this->classe ) ) ? 'X': $this->classe;
        return $implantation_code . '-' . $this->code_pe . '-' . $this->annee . '-' . $classe;
    }

    public function getLabelYearAttribute()
    {
        $implantation_code = $this->implantation->code;
        return $implantation_code . '-' . $this->code_pe . '-' . $this->annee;
    }

    public function getLabelClassAttribute()
    {
        $classe = ( is_null( $this->classe ) ) ? 'X': $this->classe;
        return $this->code_pe . '-' . $this->annee . '-' . $classe;
    }

    public function getAaIdsAttribute()
    {
        $aa_ids = [];
        if ($this->has_aa()) {
            $aa_ids = explode(';', $this->cours_ue);
        }

        return $aa_ids;
    }

    private function _add_active_filter($rqs, $filter, $table)
    {
        $academic_year = \AcademicYearManager::current();

        if ($filter == 'active')
            $rqs->where("$table.annee_academique", '=', $academic_year);
        else if ($filter == 'inactive')
            $rqs->where("$table.annee_academique", '<', $academic_year);
    }

    private function _set_identity($datas, $type)
    {
        if( ! is_null($datas) )
        {
            if (get_class($datas) == 'Illuminate\Database\Eloquent\Collection')
            {
                $typed = $datas->map(function ($item, $key) use ($type) {
                    return $item->identity = $type;
                });
            }
            else // his a model
            {
                $datas->identity = $type;
            }

        }
        return $datas;
    }
}

