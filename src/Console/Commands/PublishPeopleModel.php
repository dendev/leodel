<?php

namespace Dendev\Leodel\Console\Commands;

use Illuminate\Console\GeneratorCommand;

class PublishPeopleModel extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leodel:publish_people_model';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish the People model to App\Models\People';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/../../Models/People.php';
    }

    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function handle()
    {
        $destination_path = $this->laravel['path'].'/Models/People.php';

        if ($this->files->exists($destination_path)) {
            $this->error('People model already exists!');

            return false;
        }

        $this->makeDirectory($destination_path);

        $this->files->put($destination_path, $this->buildClass());

        $this->info($this->laravel->getNamespace().'Models\PeopleUser.php created successfully.');
    }

    /**
     * Build the class. Replace Leodel namespace with App one.
     *
     * @param string $name
     *
     * @return string
     */
    protected function buildClass($name = false)
    {
        $stub = $this->files->get($this->getStub());

        return $this->makeReplacements($stub);
    }

    /**
     * Replace the namespace for the given stub.
     * Replace the User model, if it was moved to App\Models\User.
     *
     * @param string $stub
     * @param string $name
     *
     * @return string|string[]
     */
    protected function makeReplacements(&$stub)
    {
        $stub = str_replace('Dendev\Leodel\Models;', $this->laravel->getNamespace().'Models;', $stub);

        if (! $this->files->exists($this->laravel['path'].'/Models/People.php')) {
            $stub = str_replace($this->laravel->getNamespace().'People', $this->laravel->getNamespace().'Models\People', $stub);
        }

        return $stub;
    }
}
