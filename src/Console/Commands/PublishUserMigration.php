<?php

namespace Dendev\Leodel\Console\Commands;

use Illuminate\Console\GeneratorCommand;

class PublishUserMigration extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leodel:publish_user_migration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish migration who add people_id to users table';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/../../../database/migrations/2021_08_17_203655_add_field_people_id_to_users.php';
    }

    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function handle()
    {
        $destination_path = $this->laravel['path'].'/../database/migrations/2021_08_17_203655_add_field_people_id_to_users.php';

        if ($this->files->exists($destination_path)) {
            $this->error('user migration already exists!');

            return false;
        }

        $this->makeDirectory($destination_path);

        $this->files->put($destination_path, $this->buildClass());

        $this->info('User people migration created successfully.');
    }

    /**
     * Build the class. Replace Leodel namespace with App one.
     *
     * @param string $name
     *
     * @return string
     */
    protected function buildClass($name = false)
    {
        $stub = $this->files->get($this->getStub());

        return $this->makeReplacements($stub);
    }

    /**
     * Replace the namespace for the given stub.
     * Replace the User model, if it was moved to App\Models\User.
     *
     * @param string $stub
     * @param string $name
     *
     * @return string|string[]
     */
    protected function makeReplacements(&$stub)
    {
        return $stub;
    }
}
