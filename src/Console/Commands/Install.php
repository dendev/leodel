<?php

namespace Dendev\Leodel\Console\Commands;

use Illuminate\Console\Command;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leodel:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installation automatique du package Leodel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->publish_config();
        $this->about_config();

        $this->publish_migration();
        $this->migrate();
        $this->about_migration();
    }

    public function publish_config()
    {
        $this->call('vendor:publish', ['--provider' => 'Dendev\Leodel\AddonServiceProvider', '--tag' => 'config']);
        $this->info('[Leodel] Publication de la config dans config/leodel.php');
        $this->info('');
    }

    public function about_config()
    {
        $configs = [
            ['DB_SHELDON_CONNECTION', 'sheldon'],
            ['DB_SHELDON_HOST','ip_server'],
            ['DB_SHELDON_PORT','5432'],
            ['DB_SHELDON_DATABASE','postgres'],
            ['DB_SHELDON_SCHEMA','brain'],
            ['DB_SHELDON_USERNAME','username'],
            ['DB_SHELDON_PASSWORD','password'],
        ];
        $this->info("[Leodel] Reste à ajouter les variables suivantes dans le .env" );
        $this->info('');

        foreach( $configs as $config )
        {
            $key = $config[0];
            $value = $config[1];

            $config_str = "{$key}=$value";
            $this->info($config_str);
        }
    }

    public function publish_migration()
    {
        $this->call('leodel:publish_user_migration');
        $this->info('[Leodel] Publication de la migration 2021_08_17_203655_add_field_people_id_to_users dans database/migrations/');
        $this->info('');
    }

    public function migrate()
    {
        $this->call('migrate');
        $this->info('[Leodel] php artisan migrate effectué');
        $this->info('');
    }

    public function about_migration()
    {
        $this->info("[Leodel] Pour faire le lien entre user Laravel et Sheldon il faut dans le model User, ajouter le trait UserPeople." );
        $this->info("use UserPeople dans la class User");
        $this->info('');
    }
}
