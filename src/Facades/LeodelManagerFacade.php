<?php

namespace Dendev\Leodel\Facades;

use Illuminate\Support\Facades\Facade;

class LeodelManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'leodel_manager';
    }
}
