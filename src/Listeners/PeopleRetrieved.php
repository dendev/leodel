<?php


namespace Dendev\Leodel\Listeners;

use Dendev\Leodel\Events\PeopleRetrieved as PeopleRetrievedEvent;
use Dendev\Leodel\Models\Administrative;
use Dendev\Leodel\Models\External;
use Dendev\Leodel\Models\Student;
use Dendev\Leodel\Models\Teacher;

class PeopleRetrieved
{
    public function handle(PeopleRetrievedEvent $event)
    {
        $personas = [];

        $people= $event->people;

        // FIXME persona actif ou pas peut importe ??
        if( Teacher::where('id_people', $people->id_people)->count() > 0 )
        {
            $personas[] = 'teacher';
        }

        if( Student::where('id_people', $people->id_people)->count() > 0 )
        {
            $personas[] = 'student';
        }

        if( Administrative::where('id_people', $people->id_people)->count() > 0 )
        {
            $personas[] = 'administrative';
        }

        if( External::where('id_people', $people->id_people)->count() > 0 )
        {
            $personas[] = 'external';
        }


        $people->personas = $personas;
    }

}
