<?php

namespace Dendev\Leodel;

use Dendev\Leodel\Console\Commands\Install;
use Dendev\Leodel\Console\Commands\PublishPeopleModel;
use Dendev\Leodel\Console\Commands\PublishUserMigration;
use Illuminate\Support\ServiceProvider;

class AddonServiceProvider extends ServiceProvider
{
    use AutomaticServiceProvider;

    protected $vendorName = 'dendev';
    protected $packageName = 'leodel';
    protected $commands = [Install::class, PublishPeopleModel::class, PublishUserMigration::class];
}
