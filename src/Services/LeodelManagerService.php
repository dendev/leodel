<?php

namespace Dendev\Leodel\Services;


use Dendev\Leodel\Models\Implantation;
use Dendev\Leodel\Models\Lesson;
use Dendev\Leodel\Models\Orientation;
use Dendev\Leodel\Models\People;
use Dendev\Leodel\Traits\Aliasing;

/**
 * Ce service permet d'obtenir de manière directe des ensembles de données précis.
 *
 * C'est une interfaces destiné à répondre à des demandes spécifiques et réccurentes et à servire de couche d'abstraction.
 * Il n'est pas necessaire de savoir comment l'information est organisé pour utiliser ce service.
 *
 * Class LeodelManagerService
 * @package Dendev\Leodel
 */
class LeodelManagerService
{
    use Aliasing;

    // Find
    /**
     * Retourne le people dont l'id est passé
     *
     * @param string $id_people id du people
     * @return People|null Model People si trouvé sinon Null
     */
    public function find_people(string $id_people)
    {
        return People::find($id_people);
    }

    /**
     * Alias find_people_by_numproeco
     *
     * @see find_people_by_numproeco
     *
     * @param string $numproeco numéro proeco du people que l'on veut obtenir
     * @return People|null Model People si trouvé sinon Null
     */
    public function find_people_by_matricule($numproeco)
    {
        return $this->find_people_by_numproeco($numproeco);
    }

    /**
     * Fournit le people dont le numero proeco est donnée
     *
     * @param string $numproeco id proeco du people que l'on veut obtenir
     * @return People|null Model People si trouvé sinon Null
     */
    public function find_people_by_numproeco($numproeco)
    {
        return People::where('matricule', $numproeco)->first();
    }

    /**
     * Fournit le people dont l'email henallux est donnée
     *
     * @param string $email email du people que l'on veut obtenir
     * @return People|null Model People si trouvé sinon Null
     */
    public function find_people_by_email($email)
    {
        $identity = explode('@', $email)[0];
        return People::where('identifiant', $identity)->first();
    }

    /**
     * Fournit le people dont le numero proeco est donnée
     *
     * ! donne la première occurence, ne gère pas le cas ou deux personnes ont le mm nom et prénom
     *
     * @param string $name nom du people que l'on veut obtenir
     * @param string $firstname prénom du people que l'on veut obtenir
     * @return People|null Model People si trouvé sinon Null
     */
    public function find_people_by_name_and_firstname($name, $firstname)
    {
        return People::where('nom', 'like', strtoupper($name))
            ->where('prenom', 'like', $firstname)
            ->first();
    }

    // Get
    //  //  aa ( Lesson )
    /**
     * Retourne les AA de l'orientation et des ues fournis en argument
     *
     * AA est une notion extraite de la table cours.
     *
     * @param string|Orientation $id_or_model id de l'orientation ou le model orientation
     * @param array $ue_ids tableau des ids des ue
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @return Collection
     */
    public function get_aas_of_orientation_and_ues($id_or_model, $ue_ids, $filter = 'active')
    {
        $aas = collect();
        $orientation = $this->_instantiate_if_id($id_or_model, Orientation::class);

        // make cond
        $wheres = [];
        foreach( $ue_ids as $ue_id)
            $wheres[] = ['column' => 'id_cours', 'operator' => '=', 'value' => $ue_id];

        // get ues of orientation and respect wheres cond
        $ues = $orientation->get_ues($filter, $wheres);

        // get aas of correct ues
        foreach( $ues as $ue )
            $aas = $aas->merge($ue->get_aas());

        return $aas;
    }

    //  // attributions

    // //   blocs ( Lesson )
    /**
     * Fournit tous les blocs de l'orientation fournit
     *
     * Bloc est une notion extraite de la table cours et retourne des models Lesson
     *
     * @param mixed $id_or_model de l'orientation ou l'orientation
     * @return Collection liste des blocs
     */
    public function get_blocs_of_orientation( $id_or_model )
    {
        $orientation = $this->_instantiate_if_id($id_or_model, Orientation::class);

        $blocs = $orientation->get_blocs();

        return $blocs;
    }

    /**
     * Fournit toutes les blocs des orientations dont l'id est donnée
     *
     * Bloc est une notion extraite de la table cours et retourne des models Lesson
     *
     * @param array $orientations_ids ids des orientations
     * @return Collection liste des lessons
     */
    public function get_blocs_of_orientations( $orientations_ids )
    {
        $blocs = collect();

        foreach( $orientations_ids as $orientation_id )
        {
            $tmp = $this->get_blocs_of_orientation($orientation_id);
            $blocs = $blocs->merge($tmp);
        }

        return $blocs;
    }

    // Classes ( Lesson )
    /**
     * Fournit tous les classes de l'orientation fournit
     *
     * Classe est une notion extraite de la table cours et retourne des models Lesson
     *
     * @param mixed $id_or_model de l'orientation ou l'orientation
     * @return Collection liste des lessons
     */
    public function get_classes_of_orientation( $id_or_model )
    {
        $orientation = $this->_instantiate_if_id($id_or_model, Orientation::class);

        $classes = $orientation->get_classes();

        return $classes;
    }

    /**
     * Fournit toutes les blocs des orientations dont l'id est donnée
     *
     * Bloc est une notion extraite de la table cours et retourne des models Lesson
     *
     * @param array $orientations_ids ids des orientations
     * @return Collection liste des lessons
     */
    public function get_classes_of_orientations( $orientations_ids )
    {
        $classes = collect();

        foreach( $orientations_ids as $orientation_id )
        {
            $tmp = $this->get_classes_of_orientation($orientation_id);
            $classes = $classes->merge($tmp);
        }

        return $classes;
    }

    /**
     * Fournit toutes les classes de l'orientation et année fournit
     *
     * Classe est une notion extraite de la table cours et retourne des models Lesson
     *
     * @param array $orientation_ids tableau des ids des orientations
     * @param array $year_values tableau des nom des années
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @return Collection liste des classes sous forme de collection de Lesson
     */
    public function get_classes_of_orientations_and_years(  $orientation_ids, $year_values, $filter = 'active' )
    {
        $rqs = Lesson::whereIn('id_orientation', $orientation_ids)
            ->whereIn('annee', $year_values);

        $rqs = $this->_add_active_filter($rqs, $filter);

        $classes = $rqs->get();

        return $classes;
    }

    // Department
    /**
     * Retroune une liste des departements de l'utilisateur
     *
     * @param string|User $id_or_model id ou model user
     * @return \Illuminate\Support\Collection liste des departements
     */
    public function get_departments_of_user($id_or_model)
    {
        return $this->_get_resources_of_user('department', $id_or_model);
    }

    // Domain
    /**
     * Retroune une liste des domains de l'utilisateur
     *
     * @param string|User $id_or_model id ou model user
     * @return \Illuminate\Support\Collection liste des domaines
     */
    public function get_domains_of_user($id_or_model)
    {
        return $this->_get_resources_of_user('domain', $id_or_model);
    }

    // Implantations
    /**
     * Retourne la liste des implantations de l'utilisateur.
     *
     * @param mixed $id_or_model id ou model de l'utilisateur
     * @return Collection liste des implantations
     */
    public function get_implantations_of_user( $id_or_model )
    {
        return $this->_get_resources_of_user('implantation', $id_or_model);
    }

    // Lessons
    /**
     * Fournit toutes les lessons de l'orientation fournit
     *
     * @param mixed $id_or_model id ou model Orientation
     * @return Collection liste des lessons
     */
    public function get_lessons_of_orientation( $id_or_model )
    {
        $orientation = $this->_instantiate_if_id($id_or_model, Orientation::class);

        $lessons = $orientation->get_lessons();

        return $lessons;
    }

    /**
     * Fournit toutes les lessons des orientations dont l'id est donnée
     *
     * @param array $orientations_ids ids des orientations
     * @return Collection liste des lessons
     */
    public function get_lessons_of_orientations( $orientations_ids )
    {
        $lessons = collect();

        foreach( $orientations_ids as $orientation_id )
        {
            $tmp = $this->get_lessons_of_orientation($orientation_id);
            $lessons = $lessons->merge($tmp);
        }

        return $lessons;
    }

    // Orientations
    /**
     * Retourne toutes les orientations confondus de l'utilisateur.
     *
     * @param mixed $id_or_model id ou model de l'utilisateur
     * @return \Illuminate\Support\Collection lise des orientations
     */
    public function get_orientations_of_user($id_or_model)
    {
        return $this->_get_resources_of_user('orientation', $id_or_model);
    }

    /**
     * Donne les orientations de l'implantation fournit
     *
     * @param mixed $id_or_model id ou model de l'implantation
     * @return Collection liste des orientations de l'implantation
     */
    public function get_orientations_of_implantation( $id_or_model )
    {
        $implantation = $this->_instantiate_if_id($id_or_model, Implantation::class);

        $orientations = $implantation->get_orientations('active', false);

        return $orientations;
    }

    /**
     * Donne les orientations des implantations dont l'id est fournit.
     *
     * @param array $implantations_ids ids des implantations dont on veut les orientations
     * @return Collection liste des orientations
     */
    public function get_orientations_of_implantations( $implantations_ids )
    {
        $orientations = collect();

        foreach( $implantations_ids as $implantation_id )
        {
            $tmp = $this->get_orientations_of_implantation($implantation_id);
            $orientations = $orientations->merge($tmp);
        }

        return $orientations;
    }

    // Sector
    /**
     * Retroune une liste des secteurs de l'utilisateur
     *
     * @param string|User $id_or_model id ou model user
     * @return \Illuminate\Support\Collection liste des secteurs
     */
    public function get_sectors_of_user($id_or_model)
    {
        return $this->_get_resources_of_user('sector', $id_or_model);
    }

    // UE ( Lesson )
    /**
     * Renvoi les ues coorespondantes à l'orientation et les années et blocs fournis en arguments.
     *
     * UE est une notion extraite de la table cours.
     *
     * @param string|Orientation $id_or_model id de l'orientation ou le model orientation
     * @param array $year_values tableau de noms des l'années ex 1M
     * @param array $bloc_values tableau de noms des blocs ex MSIIA
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @return Collection liste des ues sous forme de Lesson
     */
    public function get_ues_of_orientation_and_years_and_blocs($id_or_model, $year_values, $bloc_values, $filter = 'active')
    {
        $orientation = $this->_instantiate_if_id($id_or_model, Orientation::class);

        $wheres = [];
        foreach( $year_values as $year_value )
            $wheres[] = ['column' => 'annee', 'operator' => '=', 'value' => $year_value];
        foreach( $bloc_values as $bloc_value )
            $wheres[] = ['column' => 'code_pe', 'operator' => '=', 'value' => $bloc_value];

        $ues = $orientation->get_ues($filter, $wheres);

        return $ues;
    }

    // Years ( Lesson )
    /**
     * Fournit tous les years de l'orientation fournit
     *
     * Year est une notion extraite de la table cours et retourne des models Lesson
     *
     * @param mixed $id_or_model de l'orientation ou l'orientation
     * @return Collection liste des lessons
     */
    public function get_years_of_orientation( $id_or_model )
    {
        $orientation = $this->_instantiate_if_id($id_or_model, Orientation::class);

        $blocs = $orientation->get_years();

        return $blocs;
    }

    /**
     * Fournit toutes les blocs des orientations dont l'id est donnée
     *
     * Bloc est une notion extraite de la table cours et retourne des models Lesson
     *
     * @param array $orientations_ids ids des orientations
     * @return Collection liste des lessons
     */
    public function get_years_of_orientations( $orientations_ids )
    {
        $years = collect();

        foreach( $orientations_ids as $orientation_id )
        {
            $tmp = $this->get_years_of_orientation($orientation_id);
            $years = $years->merge($tmp);
        }

        return $years;
    }

    // People
    /**
     * Indique si le people existe dans Sheldon
     *
     * @param string $id_people id du people
     * @return bool True si existe sinon False
     */
    public function people_exist(string $id_people): bool
    {
        $people = $this->find_people($id_people);
        return is_null($people) ? false : true;
    }

    // other
    public function test_me()
    {
        return 'leodel_manager';
    }

    // -
    /**
     * Crée une instance du model si l'argument est un id
     *
     * @param mixed $id_or_model id ou model
     * @param string $classname nom de la classe à instancié
     * @return Model retourne un objet de type model
     */
    private function _instantiate_if_id($id_or_model, $classname)
    {
        if( is_object($id_or_model) && get_class($id_or_model) == $classname )
        {
            $model = $id_or_model;
        }
        else
        {
            $model = $classname::find($id_or_model);
        }

        return $model;
    }

    /**
     * Ajoute un filtre sur la requete afin de selectionner les réusltat actif ou inactif
     *
     * @param Builder $rqs request sql
     * @param string $filter filtre la liste pour récupérer les personas active,inactive,all
     * @return Builder $rqs request sql avec clause where ajouter
     */
    private function _add_active_filter($rqs, $filter)
    {
       // dd( $rqs);
        $academic_year = \AcademicYearManager::current();
        if ($filter == 'active')
            $rqs->where('annee_academique', '=', $academic_year);
        else if ($filter == 'inactive')
            $rqs->where('annee_academique', '<', $academic_year);

        return $rqs;
    }

    /**
     * Retourne une liste des ressources demandés
     *
     * Methode core, elle sert à offire un service générique à toutes les autres methodes qui la spécialise, la précise.
     * A manipuler avec prudence car impacte toute le reste.
     *
     * @param string $resource_type nom de la ressource désirée ( implantation, orientation, department, domain, sector )
     * @param string|User $id_or_model id ou model
     * @return \Illuminate\Support\Collection liste des ressources trouvés
     * @throws \Exception Leve une erreur si une ressources n'existant pas est demandé
     */
    private function _get_resources_of_user($resource_type, $id_or_model)
    {
        // refs
        $resources_types = [
            'implantation' => ['mth' => 'get_implantations'],
            'orientation' => ['mth' => 'get_orientations'],
            'department' => ['mth' => 'get_departments'],
            'domain' => ['mth' => 'get_domains'],
            'sector' => ['mth' => 'get_sectors'],
        ];

        // continue or quit
        if( ! array_key_exists($resource_type, $resources_types) )
            throw new \Exception(__METHOD__ . "# La ressource demandé n'existe pas ( $resource_type )");

        // get mth to call
        $get_mth = $resources_types[$resource_type]['mth'];

        // get user
        $classname = config('auth.providers.users.model');
        $user = $this->_instantiate_if_id($id_or_model, $classname);

        // get resource of user
        $resources = collect();
        if( $user && $user->is_people() )
            $resources = $user->people->$get_mth('active', false);

        // end
        return $resources;
    }
}
