# Leodel

> Model's for Sheldon

## Install
```bash
composer config repositories.academic_year vcs https://gitlab.com/dendev/academic-year-package.git
composer config repositories.leodel vcs https://gitlab.com/dendev/leodel.git
composer require dendev/leodel
```

```sql
php artisan leodel:install
```

## Configuration
## DB
la commande leodel:install à dut crée un fichier de config leodel.php.   
Ce fichier configure une connection db vers Sheldon.
Il réclame des valeurs se trouvant dans le fichier .env 

ajouter les infos de connection dans le .env
```php
vim .env

DB_SHELDON_CONNECTION=sheldon
DB_SHELDON_HOST=
DB_SHELDON_PORT=5432
DB_SHELDON_DATABASE=postgres
DB_SHELDON_SCHEMA=brain
DB_SHELDON_USERNAME=
DB_SHELDON_PASSWORD=
```

## Laravel

Pour faire le lien entre user Laravel et Sheldon il faut dans le model User, ajouter le trait UserPeople.   
**Nécessite un champ people_id dans la table user**
```php
use Dendev\Leodel\Traits\UserPeople;

class User extends Model
{
    use UserPeople;
```

## Utilisation

Les methodes générale sont directement accessible dans la Facade LeodelManager.    
Pour des méthodes plus spécifiques il faut passer par les models.    

Les besoins récurrent doivent être ajouter dans La Facade Leodel pour facilier l'utilisation.

### Avec tinker ( démo )
```php
php artisan tinker
```

Récupérer un People par son id
```php
\LeodelManager::find_people(1);
```

Récupérer un People par son nom prénom 
```php
\LeodelManager::find_people_by_name_and_firstname( 'De %', 'Deni%');
```

Trouver les implantations de l'utilisateur laravel
```php
\LeodelManager::get_implantations_of_user(2)
```

```php
$p = \LeodelManagerFacade::find_people_by_name_and_firstname( 'Melc%', 'Nicola%');
$p->get_orientations_teacher();
```

## Docs

Installer [doctum](https://github.com/code-lts/doctum)
```bash
curl -O https://doctum.long-term.support/releases/latest/doctum.phar
php doctum.phar update doctum.php
google-chrome ./docs/build/index.html
```

## Test
Executer tous les tests 
```bash
phpunit 
```

Executer un jeu de test spécifique
```bash
phpunit tests/Unit/LeodelManagerTest.php
```

Executer un test spécifique
```bash
phpunit --filter testIsTeacher 
```

## Metrics

Installer [phpmetrics](https://phpmetrics.org/)
```bash
composer global require 'phpmetrics/phpmetrics'
phpmetrics --report-html=metrics ./src --junit='phpunit.xml'
google-chrome metrics/index.html
```

## Conventions
Conventions de nommage et bonne pratique pour la gestion du package [ICI](./Conventions.md)

## TODO
Les piste d'améliorations [ICI](Todo.md)
